// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class MoCapPlugin : ModuleRules
{
	public MoCapPlugin(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
		
		PublicIncludePaths.AddRange(
			new string[] {
				// ... add public include paths required here ...
			}
			);			
		
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				"UMG",
				"RWTHVRToolkit",
				"SteamVR",
				"SteamVRInputDevice",
				"HeadMountedDisplay",
				"AnimGraphRuntime",
				"CharacterPlugin",
                "ControlRig",
                "RigVM"
			}
			);
			
		
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"CoreUObject",
				"Engine",
				"Slate",
				"SlateCore",
				"Sockets",
                "Networking",
                "Json",
                "JsonUtilities",
                "UniversalLogging"
			}
			);

        if (Target.Type == TargetRules.TargetType.Editor)
        {
            PrivateDependencyModuleNames.AddRange(new string[] { "NDisplayLaunchButton" });
        }


        DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
				// ... add any modules that your module loads dynamically here ...
			}
			);
	}
}
