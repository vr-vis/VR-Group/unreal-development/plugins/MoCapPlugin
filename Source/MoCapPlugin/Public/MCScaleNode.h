// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BoneControllers/AnimNode_SkeletalControlBase.h"
#include "VirtualHuman.h"
#include "MCDefines.h"
#include "MCPawn.h"
#include "KismetAnimationLibrary.h"
#include "MCScaleNode.generated.h"

USTRUCT(BlueprintType)
struct MOCAPPLUGIN_API FMCScaleNode : public FAnimNode_SkeletalControlBase
{
	GENERATED_BODY()
	
    FBoneReference BoneToModify;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links, meta = (PinShownByDefault))
    AMCPawn* pawn;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links, meta = (PinShownByDefault))
    FBodyProportionStruct measurements;

    virtual void EvaluateSkeletalControl_AnyThread(FComponentSpacePoseContext& Output, TArray<FBoneTransform>& OutBoneTransforms) override;
    virtual bool IsValidToEvaluate(const USkeleton* Skeleton, const FBoneContainer& RequiredBones) override;
    virtual void GatherDebugData(FNodeDebugData& DebugData) override;

    FMCScaleNode();

private:

    virtual void InitializeBoneReferences(const FBoneContainer& RequiredBones) override;
};
