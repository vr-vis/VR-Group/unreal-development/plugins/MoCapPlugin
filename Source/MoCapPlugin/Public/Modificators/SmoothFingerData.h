// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Modificators/RecordingModificator.h"

#include "SmoothFingerData.generated.h"

/**
 * 
 */
UCLASS()
class MOCAPPLUGIN_API ASmoothFingerData : public ARecordingModificator
{
	GENERATED_BODY()
public:
	// Sets default values for this actor's properties
	ASmoothFingerData();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	float smoothFloat(TArray<float> values);

	TArray<float> getFingerList(EHandPart hp, bool rightHand, int step);

	void smoothHand(bool rightHand, int step);

public:
	virtual void ApplyModification();

	UFUNCTION(BlueprintCallable)
	void smooth();
};
