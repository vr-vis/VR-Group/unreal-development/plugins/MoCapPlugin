// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Modificators/ImproveRecordingModificator.h"

#include <math.h>

#include "RemovePeakRecordingModificator.generated.h"


/**
 * 
 */
UCLASS()
class MOCAPPLUGIN_API ARemovePeakRecordingModificator : public AImproveRecordingModificator
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ARemovePeakRecordingModificator();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	void SetAnimSaveState(FAnimSaveState* ass);

	virtual void ApplyModification();

	UFUNCTION(BlueprintCallable)
	void showModification();

	TArray<FManipulation>*  getModification();

	TArray< TArray<int>> getPositionJumps();

	TArray< TArray<int>> getRotationJumps();

	TArray< TArray<int>> getFingerJumps(bool rightHand);

	UFUNCTION(BlueprintCallable)
	FTimespan getMaximalSliceLength();

	UFUNCTION(BlueprintCallable)
	void setMaximalSliceLength(FTimespan ts);

	UFUNCTION(BlueprintCallable)
	FTimespan getTimespanFromTicks(int Ticks);

	UFUNCTION(BlueprintCallable)
	int getTicksFromTimespan(FTimespan ts);

	void reloadParams();

	UFUNCTION(BlueprintCallable)
	void eliminateJumps();

	void unsetAss();

protected:

	void setASS();

	void copyStepWithDifferentTime(int posCopy, int posPaste, FTimespan time);

	TArray<int> findPositionJumps(EBodyPart bp);

	TArray<int> findRotationJumps(EBodyPart bp);

	TArray<int> findFingerJumps(EHandPart hp, bool rightHand);

	float calculateMedian(TArray<float> a);

	void setBodyPartLimits();

	void setFingerPartLimits();

	int findRangeWithoutBPJumps(int JumpPos, int rangeLength, EBodyPart bp, int lengthJumpBlock = 1);

	int findRangeWithoutFingerJumps(int JumpPos, int rangeLength, EHandPart hp, bool rightHand, int lengthJumpBlock = 1);

	void eliminateBodypartJumps(TArray<TArray<int>> jumps, bool positon = true);

	void eliminateFingerJumps(bool rightHand);

	void smoothOneBodyPartData(int start, int length, EBodyPart bp);

	void smoothOneFingerData(int start, int length, EHandPart hp, bool rigthHand);

	float calculateCost(int length, float wholeDeviation, float startDeviation = 0, float endDeviation = 0);

	TTuple<int, int> getBestPositionAndLengthForSmoothing(int JumpPos, int rangeLength, bool body, bool rightHand, bool position, EBodyPart bp, EHandPart hp);

	bool checkDeviation(float deviation, bool body, bool rightHand, bool position, EBodyPart bp, EHandPart hp);

protected:

	FAnimSaveState modifiedASS;

	FAnimSaveState* originalASS;

	bool workOnProductive = true;

	bool assSet = false;

	bool Inited = false;

public:

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Maximal Time that can be used for smoothing", Category = "Modification For Improving Gestures"))
	FTimespan MaximalSliceLength = FTimespan::FromMicroseconds(100);

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Set Maximal Translation Vector Length manually and not automatically", Category = "Modification For Improving Gestures"))
	bool SetMaximalTranslationVectorLengthManually = false;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Factor Median multiplied for calculating Maximal Translation Vector Length", Category = "Modification For Improving Gestures", EditCondition = "!SetMaximalTranslationVectorLengthManually", EditConditionHides))
	float FactorOfMedianForMaximalTranslationVectorLength = 3 * -1 / (sqrt(2) * -0.4769); //www.mathworks.com/help/matlab/ref/rmoutliers.html

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Maximal Translation Vector Length", Category = "Modification For Improving Gestures", EditCondition = "SetMaximalTranslationVectorLengthManually", EditConditionHides))
	FBodyPartParams MaximalTranslationVectorLength;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Set Maximal Rotation Angle manually and not automatically", Category = "Modification For Improving Gestures"))
	bool SetMaximalRotationAngleManually = false;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Factor Median multiplied for calculating Maximal Translation Vector Length", Category = "Modification For Improving Gestures", EditCondition = "!SetMaximalRotationAngleManually", EditConditionHides))
	float FactorOfMedianForMaximalRotationAngle = 3 * -1 / (sqrt(2) * -0.4769); //www.mathworks.com/help/matlab/ref/rmoutliers.html

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Maximal Rotation Angle (in radiance)", Category = "Modification For Improving Gestures", EditCondition = "SetMaximalRotationAngleManually", EditConditionHides))
	FBodyPartParams MaximalRotationAngle;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Set Maximal Finger Movement per Step manually and not automatically", Category = "Modification For Improving Gestures"))
	bool SetMaxFingerMovementManually = false;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Factor Median multiplied for calculating Maximal Finger Movement per Step", Category = "Modification For Improving Gestures", EditCondition = "!SetMaxFingerMovementManually", EditConditionHides))
	float FactorOfMedianForMaximalFingerMovement = 3 * -1 / (sqrt(2) * -0.4769); //www.mathworks.com/help/matlab/ref/rmoutliers.html

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Maximal Finger Movement per Step for right hand", Category = "Modification For Improving Gestures", EditCondition = "SetMaxFingerMovementManually", EditConditionHides))
	FFingerParams MaximalRightHandFingerMovementPerStep;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Maximal Finger Movement per Step for left hand", Category = "Modification For Improving Gestures", EditCondition = "SetMaxFingerMovementManually", EditConditionHides))
	FFingerParams MaximalLeftHandFingerMovementPerStep;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Factor for number of additional Steps for cost function", Category = "Modification For Improving Gestures"))
	float stepCost = 0;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Factor for change from Start to end divided by number of Steps for cost function", Category = "Modification For Improving Gestures"))
	float stepSizeCost = 0;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Include Jumps to Start of and from End of Modification to cost function?", Category = "Modification For Improving Gestures"))
	bool useStartAndEndJumpsForCosts = true;
};
