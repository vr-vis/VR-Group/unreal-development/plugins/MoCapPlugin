// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Modificators/RecordingModificator.h"

#include <math.h>

#include "ImproveRecordingModificator.generated.h"

UCLASS()
class MOCAPPLUGIN_API AImproveRecordingModificator : public ARecordingModificator
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AImproveRecordingModificator();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	void setPartsToModify(bool pos, bool rot, bool finger);

	UFUNCTION(BlueprintCallable)
	bool getPartsToModify(bool pos, bool rot, bool finger);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Smoothing of Positions is part of Modification", Category = "Improve Gesture Part"))
	bool smoothPosition = true;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Smoothing of Rotations is part of Modification", Category = "Improve Gesture Part"))
	bool smoothRotation = true;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Smoothing of Finger Data is part of Modification", Category = "Improve Gesture Part"))
	bool smoothFinger = true;
};

