// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Modificators/RecordingModificator.h"

#include "ManuCreateRecordingModificator.generated.h"

/**
 * 
 */
UCLASS()
class MOCAPPLUGIN_API AManuCreateRecordingModificator : public ARecordingModificator
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AManuCreateRecordingModificator();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void ApplyModification();

	UFUNCTION(BlueprintCallable)
	void GenerateAndExecuteManipulations(int start, int end, int minLength, int maxLength, TMap<FString, bool> bodyParts, int numberOfManipulations);

	TArray<FManipulation> generateManipulations(int start, int end, int minLength, int maxLength, TMap<FString, bool> bodyParts, int numberOfManipulations);

protected:
	FManipulation generateOneManipulation(int start, int end, int minLength, int maxLength, TMap<FString, bool> bodyParts);

	FManipulation generatePositionManipulation(int start, int end, int minLength, int maxLength, TMap<FString, bool> bodyParts);

	FManipulation generateRotationManipulation(int start, int end, int minLength, int maxLength, TMap<FString, bool> bodyParts);

	void setPosLengthBodyPart(FManipulation* mani, int start, int end, int minLength, int maxLength, TMap<FString, bool> bodyParts);

	UFUNCTION(BlueprintCallable)
	TMap<FString, bool> getBodyPartsToManipulate();

	UFUNCTION(BlueprintCallable)
	void setBodyPartsToManipulate(TMap<FString, bool> bptm);

	void addSteps(int position, FTimespan timeLength);

public:

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Generate Position Manipulations", Category = "Position Manipulation"))
	bool manipulatePositions = true;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Minimal Vector Length", Category = "Position Manipulation", EditCondition = "manipulatePositions", EditConditionHides))
	float minVectorLength = 0.0;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Maximal Vector Length", Category = "Position Manipulation", EditCondition = "manipulatePositions", EditConditionHides))
	float maxVectorLength = 30.0;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Generate Rotation Manipulations", Category = "Rotation Manipulation"))
	bool manipulateRotations = true;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Minimal Rotation Angle", Category = "Rotation Manipulation", EditCondition = "manipulateRotations", EditConditionHides))
	float minAngle = 0.0;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Maximal Rotation Angle", Category = "Rotation Manipulation", EditCondition = "manipulateRotations", EditConditionHides))
	float maxAngle = 20.0;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "BodyParts to Manipulate", Category = "Manipulation"))
	TMap<FString, bool> bodyPartsToManipulate;


	UPROPERTY(EditAnywhere, meta = (DisplayName = "Use Idle Animation when shifting Time", Category = "Time Shift Manipulation"))
	bool idleForTimeShift = true;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Maximal Steps for creating Idle Animations forwards and backwards", Category = "Time Shift Manipulation", EditCondition = "idleForTimeShift", EditConditionHides))
	int maxStepsForIdleAnim = 10;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Manipulations randomly or manually set", Category = "DataManipulation"))
	bool setManualManipulations;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "List of Manipulations", Category = "DataManipulation", EditCondition = "setManualManipulations", EditConditionHides))
	TArray<FManipulation> ManipulationList;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Earliest Position to Manipulate", Category = "Position and Length", EditCondition = "!setManualManipulations", EditConditionHides))
	int Start = -1;
	UPROPERTY(EditAnywhere, meta = (DisplayName = "Latest Position to Manipulate", Category = "Position and Length", EditCondition = "!setManualManipulations", EditConditionHides))
	int End = -1;
	UPROPERTY(EditAnywhere, meta = (DisplayName = "Minimal Block Length of Manipualtion", Category = "Position and Length", EditCondition = "!setManualManipulations", EditConditionHides))
	int MinLength = 10;
	UPROPERTY(EditAnywhere, meta = (DisplayName = "Maximal Block Length of Manipualtion", Category = "Position and Length", EditCondition = "!setManualManipulations", EditConditionHides))
	int MaxLength = 100;
	UPROPERTY(EditAnywhere, meta = (DisplayName = "Number of Manipualtion to Generate", Category = "Position and Length", EditCondition = "!setManualManipulations", EditConditionHides))
	int NumberOfManipulations = 10;

};
