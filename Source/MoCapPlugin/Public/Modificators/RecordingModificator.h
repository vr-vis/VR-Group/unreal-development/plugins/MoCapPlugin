// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "MCDefines.h"


#include "RecordingModificator.generated.h"


UENUM()
enum FModType {
	TimeShift = 0 UMETA(DisplayName = "Shift the Time"),
	RotationShift = 1 UMETA(DisplayName = "Rotation Offset"),
	PositionShift = 2 UMETA(DisplayName = "Translation Vector"),
	FingerShift = 3 UMETA(DisplayName = "FingerDataShift"),
	DuplicateStep = 4 UMETA(DisplayName = "Duplicate Step"),
	SetPosition = 5 UMETA(DisplayName = "Set Position"),
	SetRotation = 6 UMETA(DisplayName = "Set Rotation")
};

UENUM()
enum EHandPart {
	Thumb = 0,
	Index = 1,
	Middle = 2,
	Ring = 3,
	Pinky = 4,
	Thumb_Index = 5,
	Index_Middle = 6,
	Middle_Ring = 7,
	Ring_Pinky = 8,
	Hand_LAST = 9
};


USTRUCT()
struct FManipulation {
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	TEnumAsByte<FModType> Typ;

	UPROPERTY(EditAnywhere)
	int startStep = -1;

	UPROPERTY(EditAnywhere, meta = (EditCondition = "Typ!=4", EditConditionHides))
	int endStep = -1;

	UPROPERTY(EditAnywhere, meta = (EditCondition = "Typ==0", EditConditionHides))
	FTimespan duration = 0;

	UPROPERTY(EditAnywhere, meta = (EditCondition = "Typ==1 || Typ==2 || Typ == 5 || Typ == 6", EditConditionHides))
	TEnumAsByte <EBodyPart> bodyPartToShift;

	UPROPERTY(EditAnywhere, meta = (EditCondition = "Typ==1 || Typ == 6", EditConditionHides))
	FQuat rotQuat;

	UPROPERTY(EditAnywhere, meta = (EditCondition = "Typ==2 || Typ == 5", EditConditionHides))
	FVector posVec;

	UPROPERTY(EditAnywhere, meta = (EditCondition = "Typ==3", EditConditionHides))
	TEnumAsByte <EHandPart> handPartToShift;

	UPROPERTY(EditAnywhere, meta = (EditCondition = "Typ==3", EditConditionHides))
	bool rightHand;

	UPROPERTY(EditAnywhere, meta = (EditCondition = "Typ==3", EditConditionHides))
	float fingerMovement;

	FString toString() {
		switch (Typ)
		{
		case TimeShift: return ("TimeShift{ startStep: " + FString::FromInt(startStep) + "; endStep: " + FString::FromInt(endStep) + "; duration: " + duration.ToString() + "}"); 
		case RotationShift: return ("RotationShift{ startStep: " + FString::FromInt(startStep) + "; endStep: " + FString::FromInt(endStep) + "; bodyPart: " + FString::FromInt(bodyPartToShift) + "; rotQuat: " + rotQuat.ToString() + "}"); 
		case PositionShift: return ("PositionShift{ startStep: " + FString::FromInt(startStep) + "; endStep: " + FString::FromInt(endStep) + "; bodyPart: " + FString::FromInt(bodyPartToShift) + "; posVec: " + posVec.ToString() + "}"); 
		case FingerShift: return ("FingerShift{ startStep: " + FString::FromInt(startStep) + "; endStep: " + FString::FromInt(endStep) + "; handPart: " + FString::FromInt(handPartToShift) + "; rightHand: " + FString::FromInt(rightHand) + "; FingerValue: " + FString::SanitizeFloat(fingerMovement) + "}"); 
		case DuplicateStep: return ("DuplicateStep{ Step: " + FString::FromInt(startStep) + "}"); 
		case SetPosition: return ("SetPosition{ startStep: " + FString::FromInt(startStep) + "; endStep: " + FString::FromInt(endStep) + "; bodyPart: " + FString::FromInt(bodyPartToShift) + "; posVec: " + posVec.ToString() + "}");
		case SetRotation: return ("SetRotation{ startStep: " + FString::FromInt(startStep) + "; endStep: " + FString::FromInt(endStep) + "; bodyPart: " + FString::FromInt(bodyPartToShift) + "; rotQuat: " + rotQuat.ToString() + "}");
		default:
			break;
		}
		return ("Unknown Type: " + FString::FromInt(Typ));
	}
};

USTRUCT()
struct FFingerParams {
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, meta = (DisplayName = "Setting for Thumb"))
		float Thumb = -1;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Setting for Index"))
		float Index = -1;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Setting for Middle"))
		float Middle = -1;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Setting for Ring"))
		float Ring = -1;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Setting for Pinky"))
		float Pinky = -1;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Setting for Thumb_Index"))
		float Thumb_Index = -1;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Setting for Index_Middle"))
		float Index_Middle = -1;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Setting for Middle_Ring"))
		float Middle_Ring = -1;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Setting for Ring_Pinky"))
		float Ring_Pinky = -1;

	float getHandPartData(EHandPart hp) {
		switch (hp) {
		case EHandPart::Thumb: return Thumb;
		case EHandPart::Index: return Index;
		case EHandPart::Middle: return Middle;
		case EHandPart::Ring: return Ring;
		case EHandPart::Pinky: return Pinky;
		case EHandPart::Thumb_Index: return Thumb_Index;
		case EHandPart::Index_Middle: return Index_Middle;
		case EHandPart::Middle_Ring: return Middle_Ring;
		case EHandPart::Ring_Pinky: return Ring_Pinky;
		}
		return -1;
	}

	void setHandPartData(EHandPart hp, float value) {
		switch (hp) {
		case EHandPart::Thumb: Thumb = value; return;
		case EHandPart::Index: Index = value; return;
		case EHandPart::Middle: Middle = value; return;
		case EHandPart::Ring: Ring = value; return;
		case EHandPart::Pinky: Pinky = value; return;
		case EHandPart::Thumb_Index: Thumb_Index = value; return;
		case EHandPart::Index_Middle: Index_Middle = value; return;
		case EHandPart::Middle_Ring: Middle_Ring = value; return;
		case EHandPart::Ring_Pinky: Ring_Pinky = value; return;
		}
	}
};

USTRUCT()
struct FBodyPartParams {
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, meta = (DisplayName = "Setting for Head"))
		float paramHead = -1;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Setting for Left Hand"))
		float paramHandL = -1;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Setting for Right Hand"))
		float paramHandR = -1;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Setting for Lower Left Arm"))
		float paramLowerArmL = -1;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Setting for Lower Right Arm"))
		float paramLowerArmR = -1;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Setting for Lower Left Leg"))
		float paramLowerLegL = -1;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Setting for Lower Right Leg"))
		float paramLowerLegR = -1;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Setting for Upper Body"))
		float paramUpperBody = -1;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Setting for Lower Body"))
		float paramLowerBody = -1;


	float getBodyPartEntry(EBodyPart bp) {
		switch (bp) {
		case EBodyPart::Head:
			return paramHead;
		case EBodyPart::HandL:
			return paramHandL;
		case EBodyPart::HandR:
			return paramHandR;
		case EBodyPart::LowerArmL:
			return paramLowerArmL;
		case EBodyPart::LowerArmR:
			return paramLowerArmR;
		case EBodyPart::LowerLegL:
			return paramLowerLegL;
		case EBodyPart::LowerLegR:
			return paramLowerLegR;
		case EBodyPart::UpperBody:
			return paramUpperBody;
		case EBodyPart::LowerBody:
			return paramLowerBody;
		default:
			return -1;
		}
	}

	void setBodyPartEntry(EBodyPart bp, float value) {
		switch (bp) {
		case EBodyPart::Head:
			paramHead = value; return;
		case EBodyPart::HandL:
			paramHandL = value; return;
		case EBodyPart::HandR:
			paramHandR = value; return;
		case EBodyPart::LowerArmL:
			paramLowerArmL = value; return;
		case EBodyPart::LowerArmR:
			paramLowerArmR = value; return;
		case EBodyPart::LowerLegL:
			paramLowerLegL = value; return;
		case EBodyPart::LowerLegR:
			paramLowerLegR = value; return;
		case EBodyPart::UpperBody:
			paramUpperBody = value; return;
		case EBodyPart::LowerBody:
			paramLowerBody = value; return;
		default:
			return;
		}
	}

};

UCLASS()
class MOCAPPLUGIN_API ARecordingModificator : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ARecordingModificator();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	void SetAssAndExecuteModification(FAnimSaveState* ass, TArray<FManipulation>* trackedModifications=nullptr);

	virtual void SetAnimSaveState(FAnimSaveState* ass);

	void SetTrackedModifications(TArray<FManipulation>* trackedModifications);

	UFUNCTION(BlueprintCallable)
	virtual void ApplyModification();

	virtual void setTrackingVar(TArray<FManipulation>* trackedModifications);

	FAnimSaveState* GetAnimSaveState();

	void ExecuteMaipulation(FManipulation* manipulation);

	void ExecuteMaipulations(TArray<FManipulation>* manipulationList);

	void ShiftTime(FTimespan t, int firstStep, int lastStep);

	void ShiftRot(EBodyPart bodyPart, FQuat additionalRotQuat, int firstStep, int lastStep);

	void SetRotations(EBodyPart bodyPart, FQuat rotQuat, int firstStep, int lastStep);

	void ShiftPos(EBodyPart bodyPart, FVector additionalPosVec, int firstStep, int lastStep);

	void SetPositions(EBodyPart bodyPart, FVector posVec, int firstStep, int lastStep);

	void ShiftFinger(EHandPart handPart, float additionalMovement, bool rightHand, int firstStep, int lastStep);

	void addStep(FProcessedAnimData step, int position);

	void deleteStep(int position);

	virtual void duplicateStep(int position);

	UFUNCTION(BlueprintCallable)
	static FString getBodyPartName(EBodyPart bp);

	UFUNCTION(BlueprintCallable)
	static FString getHandPartName(EHandPart hp);

	float getHandPartData(EHandPart hp, bool rightHand, FFingerData* data);

	void setHandPartData(EHandPart hp, bool rightHand, FFingerData* data, float value);

	UFUNCTION(BlueprintCallable)
	int getAssSize();

protected:
	FAnimSaveState* ASS;

	TArray<FManipulation>* TrackedModifications;

	int prepareStart(int start);

	int prepareEnd(int end);

	FSensorDataEntry* getBodyPartSensorData(EBodyPart BodyPart, FSensorData* sensData);

	FTimespan getTimePerStep();

	virtual void addSteps(int position, FTimespan timeLength);

	virtual void deleteTimeOverlappingSteps();

	bool stepContainsMeasurements(int step);

	FString ArrayOfIntsToString(TArray<int> array);

	float getRotAngularDistance(FSensorData* step1, FSensorData* step2, EBodyPart bp);

	float getPosDistance(FSensorData* step1, FSensorData* step2, EBodyPart bp);

	float getFingerDataDistance(FFingerData* step1, FFingerData* step2, EHandPart hp, bool rightHand);

	float getDeviation(int start, int length, bool body, bool rightHand, bool position, EBodyPart bp, EHandPart hp);

	void debugMsg(FString msg, FColor color = FColor::White);

	void SetTime(FTimespan t, int step);

	FTimespan GetTime(int step);

	void SetRot(EBodyPart bodyPart, FQuat RotQuat, int step);

	FQuat GetRot(EBodyPart bodyPart, int step);

	void SetPos(EBodyPart bodyPart, FVector PosVec, int step);

	FVector GetPos(EBodyPart bodyPart, int step);

	void SetFingerData(EHandPart handPart, bool rightHand, float FingerValue, int step);

	float GetFingerData(EHandPart handPart, bool rightHand, int step);

	virtual void copyStepWithDifferentTime(int posCopy, int posPaste, FTimespan time);

public:

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Show Debug Messages on Screen", Category = "Modification"))
	bool debugMsgs = false;
};