// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Modificators/RecordingModificator.h"

#include "TimeRecordingModificator.generated.h"

/**
 * 
 */
UCLASS()
class MOCAPPLUGIN_API ATimeRecordingModificator : public ARecordingModificator
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	ATimeRecordingModificator();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UFUNCTION(BlueprintCallable)
	void ManipulateCurrentFile();

	virtual void ApplyModification();


protected:
	UFUNCTION(BlueprintCallable)
	void scaleTimeRandomly(int stepStart = -1, int stepEnd = -1);

	UFUNCTION(BlueprintCallable)
	int getMaxScale();

	UFUNCTION(BlueprintCallable)
	void setMaxScale(int maxScale);

	UFUNCTION(BlueprintCallable)
	int getStartStep();

	UFUNCTION(BlueprintCallable)
	void setStartStep(int startStep);

	UFUNCTION(BlueprintCallable)
	int getEndStep();

	UFUNCTION(BlueprintCallable)
	void setEndStep(int endStep);

	void distibuteTimeEqually();

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Maximal Factor from not modified Steps to modified steps", Category = "Time Modification"))
	int MaxScale = 5;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Start Step for Scaling", Category = "Time Modification"))
	int StartStep = -1;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "End Step for Scaling", Category = "Time Modification"))
	int EndStep = -1;
};
