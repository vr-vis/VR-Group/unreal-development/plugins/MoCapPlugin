// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Modificators/ImproveRecordingModificator.h"

#include "SmoothRecordingModificator.generated.h"

/**
 * 
 */
UCLASS()
class MOCAPPLUGIN_API ASmoothRecordingModificator : public AImproveRecordingModificator
{
	GENERATED_BODY()
	

public:
	// Sets default values for this actor's properties
	ASmoothRecordingModificator();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	FQuat smoothQuaternion(TArray<FQuat> quats);

	FVector smoothVectors(TArray<FVector> vecs);

	float smoothFloat(TArray<float> values);

	TArray<FQuat> getRotList(EBodyPart bp, int step);

	TArray<FVector> getPosList(EBodyPart bp, int step);

	TArray<float> getFingerList(EHandPart hp, bool rightHand, int step);

	void smoothRotations();

	void smoothPositions();

	void smoothFingerData(bool rightHand);


public:
	virtual void ApplyModification();
	
	UFUNCTION(BlueprintCallable)
	void smooth();

	UFUNCTION(BlueprintCallable)
	int	getSmoothWindowSize();

	UFUNCTION(BlueprintCallable)
	void setSmoothWindowSize(int value);

public:
	UPROPERTY(EditAnywhere, meta = (DisplayName = "Size of the smoothing window", Category = "Smoothing Gestures"))
	int SmoothWindowSize = 8;
};
