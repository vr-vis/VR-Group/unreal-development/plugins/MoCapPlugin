// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"

#include "Animation/PoseSnapshot.h"
#include "MCDefines.h"

#include "MCAnimInstance.generated.h"

class AMCPawn;

UCLASS()
class MOCAPPLUGIN_API UMCAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:

	int MaxWaitTicks;
	float WaitTicks;
	bool Waiting;
	bool AppliedPose;

	TArray<FSnapshotAnimations> SnapshotAnimations;

	UPROPERTY(BlueprintReadWrite)
	FSensorData SensorData;

	UPROPERTY(BlueprintReadWrite)
	FSensorOffsets SensorOffsets;

	UPROPERTY(BlueprintReadWrite)
	FAdditionalOffsets AdditionalOffsets;

	UPROPERTY(BlueprintReadWrite)
	FBodyProportionStruct Measurements;

	UPROPERTY(BlueprintReadWrite)
	AMCPawn* PawnOwner;

	UPROPERTY(BlueprintReadWrite)
	FFingerData FingerData;

	UPROPERTY(BlueprintReadWrite)
	bool DoFingers;

	UPROPERTY(BlueprintReadWrite)
	bool DoRig;

	UPROPERTY(BlueprintReadWrite)
	bool LockFeet;

	UPROPERTY(BlueprintReadWrite)
	bool LimitHandRot;

	UPROPERTY(BlueprintReadWrite)
	bool UseHandPos;

	UPROPERTY(BlueprintReadWrite)
	bool FixChest;

	UPROPERTY(BlueprintReadWrite)
	bool FixHead;

	UPROPERTY(BlueprintReadWrite)
	bool SkipCalibration;

	UPROPERTY(BlueprintReadWrite)
	bool SkipBodyPartsCalibration = false;

	UPROPERTY(BlueprintReadWrite)
	FTransform LeftFootPlane;

	UPROPERTY(BlueprintReadWrite)
	FTransform RightFootPlane;

	UPROPERTY(BlueprintReadWrite)
	FVector Scale;

	UPROPERTY(BlueprintReadWrite)
	float FingerAngleScale = 1.f;

	virtual void NativeInitializeAnimation() override;

	virtual void NativeUpdateAnimation(float DeltaSeconds) override;

	UFUNCTION(BlueprintCallable)
	const FSensorDataEntry& GetSensorData(TEnumAsByte<EBodyPart> BodyPart);

	void SetSensorData(EBodyPart BodyPart, const FVector& Pos, const FQuat& Rot);
	void SetSensorData(EBodyPart BodyPart, const FQuat& Rot);

	void SetSensorOffset(EBodyPart BodyPart, const FTransform& DeltaTransform);

};
