// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Core.h"
#include "Json.h"
#include "MCDefines.h"

class MCUtils {
public:

	static FString JsonToString(TSharedPtr<FJsonObject> Json, bool bFormat = true);
	
	static TSharedPtr<FJsonObject> StringToJson(FString String);

	static FTimespan StringToTimespan(FString TimeString);

};