// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "MCLogHandler.h"
#include "MCPawn.h"
#include "MCDefines.h"
#include "MCAnimInstance.h"
#include "MCInstructionWidget.h"
#include "MCFeedbackWidget.h"
#include "Components/WidgetComponent.h"
#include "Engine/SceneCapture2D.h"
#include "Modificators/ImproveRecordingModificator.h"

#include "MCController.generated.h"



UCLASS()
class MOCAPPLUGIN_API AMCController : public AActor
{
	GENERATED_BODY()

public:

	bool DebugMode = false;
	bool UseLastOffsets = false;
	bool SetControls = true;
	bool KeepPawnInvisible = false;
	bool KeepPawnVisible = false;
	bool KeepDoingRig = false;
	bool OutputMsgOnScreen = true;

	AMCController();
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

protected:

	virtual void BeginPlay() override;

public:	

	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void ToggleRecording();

	UFUNCTION(BlueprintCallable)
	void SetMarker();

	UFUNCTION(BlueprintCallable)
	void SaveAnimationEditor();

	UFUNCTION(BlueprintCallable)
	void NextEditFrame();

	UFUNCTION(BlueprintCallable)
	void PrevEditFrame();

	UFUNCTION(BlueprintCallable)
	void FinishEditAnim();

protected:

	void RecordMode();
	void SaveToAnimMode();

	void ScaleAnimDataInterval(int start, int end, float DeltaTime, LinearTransformType LinearTransType, float EasingExponent);
	void TranslateJsonToAnimData();
	void InterpolateOutages();
	void LegHipSmoothing();
	void ScaleHaltingAreas(float StartHaltingPoint, float EndHaltingPoint);
	void CenterBodyOnFootIndicators();
	void HipHandling();
	void ChestHandling();
	void FeetPositions();
	void TakeDefaultPose();
	bool PreprocessRecording(float StartHaltingPoint, float EndHaltingPoint);
	void ApplyModificators();
	void InputNextFrame();

	void SaveAnimation(float StartHaltingPoint, float EndHaltingPoint, bool skipTranslation = false);
	void SaveAnimSnapshots();
	UAnimSequence* SaveAsAnimSequence(const FSnapshotAnimations& Recording, const FString& AnimName, const FString& FolderName);
	void SetBonesAnimationInAnimSeq(const FSnapshotAnimations& Recording, UAnimSequence* AnimSequence);
	
#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
#endif

	TSubclassOf<AActor> InstructionWidgetObjClass;
	AActor* InstructionWidgetObj;
	UMCInstructionWidget* InstructionWidget;
	TSubclassOf<AActor> FeedbackWidgetObjClass;
	UMCFeedbackWidget* FeedbackWidget;

	UMCLogHandler LogHandler;

	FAnimSaveState AnimSaveState;
	
	bool IsRecording = false;
	bool IsSavingToAnim = false;

	bool SpectatorInited = false;

	int CurRecordingInSession = 0;

	FAdditionalOffsets LastAddOffsets;

	FString SaveToDest = "";

	float InitialPelvisFootDist = 0.;


public:

	/*------------GENERAL PROPERTIES-------------*/

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Scale Mesh", Category = "MotionCapture"))
	float Scale = -1.f;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Path to Measurement File", Category = "MotionCapture"))
	FString MeasurementPath = "Plugins/MocapPlugin/Content/Measurements.txt";

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Name of Recording", Category = "MotionCapture"))
	FString NameOfRecording = "Recording";

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Enable Finger Reproduction", Category = "MotionCapture"))
	bool bFingerTrackingEnabled = true;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Finger Tracking Method", Category = "MotionCapture"))
	ESkeletalSummaryDataType FingerTrackingMethod = ESkeletalSummaryDataType::VR_SummaryType_FromAnimation;

	/*------------MAP PROPERTIES-------------*/

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Pawn", Category = "MotionCapture Map"))
	AMCPawn* Pawn;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Spectator Cam", Category = "MotionCapture Map"))
	ASceneCapture2D* SpectatorCam;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Left Foot Plane", Category = "MotionCapture Map"))
	AActor* LeftFootPlane;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Right Foot Plane", Category = "MotionCapture Map"))
	AActor* RightFootPlane;

	/*------------ANIM PROPERTIES-------------*/

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Frames per Second", Category = "MotionCapture Anim"))
	int FramesPerSecond = 60;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Gesture Do Holding", Category = "MotionCapture Anim"))
	bool DoHolding = false;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Gesture Hold Scale Excess Start Time", Category = "MotionCapture Anim"))
	float GestureHoldExcessStartTime = 0.f;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Gesture Hold Scale Excess End Time", Category = "MotionCapture Anim"))
	float GestureHoldExcessEndTime = 0.f;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Gesture Hold Start Excess Easing Exponent", Category = "MotionCapture Anim"))
	float GestureHoldStartExcessEasingExponent = 2.f;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Gesture Hold End Excess Easing Exponent", Category = "MotionCapture Anim"))
	float GestureHoldEndExcessEasingExponent = 2.f;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Should Return To Default Pose At Start And End", Category = "MotionCapture Anim"))
	bool ReturnToDefaultPose = false;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Default Pose File Relative From Content", Category = "MotionCapture Anim"))
	FString TakeDefaultPoseFromFile = "LastDefaultPose.txt";

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Default Pose Reference Anim Time", Category = "MotionCapture Anim"))
	double TakeDefaultPoseFromTime = -1.0;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Default Pose Reference Anim Part", Category = "MotionCapture Anim"))
	int TakeDefaultPoseFromAnimPart = 0;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Default Pose Time To Interpolate To It", Category = "MotionCapture Anim"))
	float DefaultPoseTakingTime = 0.6f;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Chest Control Center", Category = "MotionCapture Anim"))
	FVector ChestControlCenter;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Chest Control Factor", Category = "MotionCapture Anim"))
	float ChestControlFactor = 0.0;

	//UPROPERTY(EditAnywhere, meta = (DisplayName = "Chest Control Keep Length", Category = "MotionCapture Anim"))
	bool ChestKeepLength = false;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Set Chest Transform After Arm IK", Category = "MotionCapture Anim"))
	bool FixChest = true;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Hip Shift To Reducing Center", Category = "MotionCapture Anim"))
	bool HipShiftToReducingCenter = false;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Hip Reducing Center", Category = "MotionCapture Anim"))
	FVector HipReducingCenter;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Hip Reducing Factor", Category = "MotionCapture Anim"))
	float HipReducingFactor = 0.0;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Nr of Leg Smoothing Iterations", Category = "MotionCapture Anim"))
	int LegSmoothTimes = 5;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Leg Smoothing Neighbor Window", Category = "MotionCapture Anim"))
	int LegSmoothWindow = 3;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Lock Feet To Green Foot Indicators", Category = "MotionCapture Anim"))
	bool LockFeet = true;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Foot Height Target", Category = "MotionCapture Anim"))
	float FootHeightTarget = 0.f;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Max Leg Length", Category = "MotionCapture Anim"))
	float MaxLegLength = 96.f;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Use Captured Hand Position", Category = "MotionCapture Anim"))
	bool UseHandPosition = true;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Limit Hand Rotation (can cause rotational jumping)", Category = "MotionCapture Anim"))
	bool LimitHandRotation = false;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Finger Angle Scale", Category = "MotionCapture Anim"))
	float FingerAngleScale = 1.f;


	//UPROPERTY(EditAnywhere, meta = (DisplayName = "Edit Offsets Mode", Category = "MotionCapture Anim"))
	bool EditOffsetMode = false;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Additional Post Processing Offsets", Category = "MotionCapture Anim"))
	FAdditionalOffsets AdditionalOffsets;


	UPROPERTY(EditAnywhere, meta = (DisplayName = "Use Modificators", Category = "MotionCapture Modification"))
	bool UseModificators = true;

	//"List of Modifications defined by Modificator Classes that are applied in order to the data
	UPROPERTY(EditAnywhere, meta = (Category = "MotionCapture Modification"))
	TArray<ARecordingModificator*> Modificators;

};
