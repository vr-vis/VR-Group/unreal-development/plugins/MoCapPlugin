// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Units/RigUnit.h"
#include "MCDefines.h"
#include "MCRigUnits.generated.h"

USTRUCT(meta = (DisplayName = "SensorOffsets", PrototypeName = "SensorOffsets", Category = "Hierarchy", NodeColor = "0.05 0.25 0.05"))
struct FRigUnit_SensorOffsets : public FRigUnit
{
	GENERATED_BODY()

	FRigUnit_SensorOffsets();

	RIGVM_METHOD()
	virtual void Execute(const FRigUnitContext& Context) override;

	UPROPERTY(meta = (Input))
	FTransform ControlTrans;

	UPROPERTY(meta = (Input))
	FSensorOffset SensorOffset;

	UPROPERTY(meta = (Output))
	FTransform OutTrans;

};

USTRUCT(meta = (DisplayName = "ApplyFingerData", PrototypeName = "ApplyFingerData", Category = "Hierarchy", NodeColor = "0.05 0.25 0.05"))
struct FRigUnit_ApplyFingerData : public FRigUnitMutable
{
	GENERATED_BODY()

	FRigUnit_ApplyFingerData();

	RIGVM_METHOD()
	virtual void Execute(const FRigUnitContext& Context) override;

	UPROPERTY(meta = (Input))
	float AngleScale;

	UPROPERTY(meta = (Input))
	FFingerData FingerData;

	UPROPERTY(meta = (Input))
	FVector ThumbAnglesOpen;

	UPROPERTY(meta = (Input))
	FVector IndexAnglesOpen;

	UPROPERTY(meta = (Input))
	FVector MiddleAnglesOpen;

	UPROPERTY(meta = (Input))
	FVector RingAnglesOpen;

	UPROPERTY(meta = (Input))
	FVector PinkyAnglesOpen;

	UPROPERTY(meta = (Input))
	FVector ThumbAnglesClosed;

	UPROPERTY(meta = (Input))
	FVector IndexAnglesClosed;

	UPROPERTY(meta = (Input))
	FVector MiddleAnglesClosed;

	UPROPERTY(meta = (Input))
	FVector RingAnglesClosed;

	UPROPERTY(meta = (Input))
	FVector PinkyAnglesClosed;

	UPROPERTY(meta = (Input))
	float Thumb_Index_Angle;

	UPROPERTY(meta = (Input))
	float Index_Middle_Angle;

	UPROPERTY(meta = (Input))
	float Middle_Ring_Angle;

	UPROPERTY(meta = (Input))
	float Ring_Pinky_Angle;

};

USTRUCT(meta = (DisplayName = "FootLocking", PrototypeName = "FootLocking", Category = "Hierarchy", NodeColor = "0.05 0.25 0.05"))
struct FRigUnit_FootLocking : public FRigUnitMutable
{
	GENERATED_BODY()

	FRigUnit_FootLocking();

	RIGVM_METHOD()
	virtual void Execute(const FRigUnitContext& Context) override;

	UPROPERTY(meta = (Input))
	float FloorOffset;

	UPROPERTY(meta = (Input))
	float ReleaseHeight;

	UPROPERTY(meta = (Input))
	float ReleaseDistHorizontal;

	UPROPERTY()
	bool LockedLeft;

	UPROPERTY()
	bool LockedRight;

	UPROPERTY()
	FTransform LockedTransLeft;

	UPROPERTY()
	FTransform LockedTransRight;

};

USTRUCT(meta = (DisplayName = "FromXYZW", PrototypeName = "FromXYZW", Category = "Math|Quaternion", NodeColor = "0.05 0.25 0.05"))
struct FRigUnit_FromXYZW : public FRigUnit
{
	GENERATED_BODY()

	FRigUnit_FromXYZW();

	RIGVM_METHOD()
		virtual void Execute(const FRigUnitContext& Context) override;

	UPROPERTY(meta = (Input))
	float X;

	UPROPERTY(meta = (Input))
	float Y;

	UPROPERTY(meta = (Input))
	float Z;

	UPROPERTY(meta = (Input))
	float W;

	UPROPERTY(meta = (Output))
	FQuat Quat;

};

USTRUCT(meta = (DisplayName = "AdditionalOffsets", PrototypeName = "AdditionalOffsets", Category = "Hierarchy", NodeColor = "0.05 0.25 0.05"))
struct FRigUnit_AdditionalOffsets : public FRigUnitMutable
{
	GENERATED_BODY()

	FRigUnit_AdditionalOffsets();

	RIGVM_METHOD()
	virtual void Execute(const FRigUnitContext& Context) override;

	UPROPERTY(meta = (Input))
	FAdditionalOffsets AdditionalOffsets;

};