// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"

class FMoCapPluginModule : public IModuleInterface
{
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

	static void AskForSettings();
	static bool GetIsMaster();
	


protected:
	static bool bPluginInitialized;
	static bool bIsMaster;
	
	
};
