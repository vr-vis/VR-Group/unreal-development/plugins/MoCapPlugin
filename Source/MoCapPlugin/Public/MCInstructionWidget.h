// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "Components/WidgetSwitcher.h"
#include "Components/Textblock.h"
#include "Kismet/GameplayStatics.h"

#include "MCInstructionWidget.generated.h"

UCLASS()
class MOCAPPLUGIN_API UMCInstructionWidget : public UUserWidget
{
	GENERATED_BODY()

public:

	int StartIndex = 0;
	int RecordIndex = 1;
	int SavingIndex = 2;

	UPROPERTY(BlueprintReadWrite)
	UWidgetSwitcher* WidgetSwitcher;

	UPROPERTY(BlueprintReadWrite)
	UTextBlock* FeedbackText;

	UMCInstructionWidget(const FObjectInitializer& ObjectInitializer);

	virtual void NativeConstruct() override;

	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

};
