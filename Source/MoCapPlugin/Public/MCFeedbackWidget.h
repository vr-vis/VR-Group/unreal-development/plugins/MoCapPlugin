// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "Components/WidgetSwitcher.h"
#include "Components/Textblock.h"
#include "Components/VerticalBox.h"
#include "Components/ProgressBar.h"
#include "Blueprint/WidgetTree.h"
#include "MCDefines.h"
#include "Kismet/GameplayStatics.h"

#include "MCFeedbackWidget.generated.h"

UCLASS()
class MOCAPPLUGIN_API UMCFeedbackWidget : public UUserWidget
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintReadWrite)
	UTextBlock* AnimNameText;

	UPROPERTY(BlueprintReadWrite)
	UTextBlock* AdditionalText;

	UPROPERTY(BlueprintReadWrite)
	UTextBlock* RecordingStateText;

	UPROPERTY(BlueprintReadWrite)
	UTextBlock* LeftFingerText;

	UPROPERTY(BlueprintReadWrite)
	UTextBlock* RightFingerText;

	UPROPERTY(BlueprintReadWrite)
	UVerticalBox* SensorBox;

	UPROPERTY(BlueprintReadWrite)
	UProgressBar* ProgBar;

	bool inited = false;


	UMCFeedbackWidget(const FObjectInitializer& ObjectInitializer);

	virtual void NativeConstruct() override;

	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	void InitSensorBox(FSensorSetup& SensorSetup);

};
