// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Camera/CameraComponent.h"
#include "MCAnimInstance.h"
#include "VirtualHuman.h"
#include "MCLogHandler.h"
#include "MCUtils.h"
#include "MCFeedbackWidget.h"
#include "SteamVRInputDeviceFunctionLibrary.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Engine/TextureRenderTarget2D.h"
#include "Components/SceneCaptureComponent2D.h"

#include "MCDefines.h"

#include "MCPawn.generated.h"

UCLASS()
class MOCAPPLUGIN_API AMCPawn : public APawn
{
	GENERATED_BODY()

public:

	bool UseKneeAsFoot;

	UPROPERTY(EditAnywhere)
	bool ShowDeviceModels = true;

	UPROPERTY(BlueprintReadOnly)
	FSensorSetup SensorSetup;

	USceneComponent* VROrigin;

	BoneNames BoneNames;

	UPROPERTY(BlueprintReadOnly)
	UCameraComponent* VRCamera;

	UPROPERTY(BlueprintReadOnly)
	USkeletalMeshComponent* SkeletalMesh;

	FSensorOffsets LastOffsets;

	AMCPawn(const FObjectInitializer& ObjectInitializer);

protected:

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	void SetBoneNames();

public:

	UFUNCTION(BlueprintCallable)
	UMCAnimInstance* GetAnimInstance();
	USkeleton* GetSkeleton();
	void SetMeshVisibility(bool visible);
	void LoadMeasurementFile(const FString& Filepath);

	void CalcSensorOffsets(UMCLogHandler& LogHandler, bool UseLastOffsets, bool DebugMode);
	void AddSensorDataToJson(TSharedPtr<FJsonObject> JsonObjectFull);
	void AddFingerDataToJson(TSharedPtr<FJsonObject> JsonObjectFull, ESkeletalSummaryDataType FingerTrackingMethod, UMCFeedbackWidget* FeedbackWidget = nullptr);
	void AICalcFrame();
	void InputViveDataToAnimInstance(TSharedPtr<FJsonObject> Data);
	void InputViveOffsetsToAnimInstance(TSharedPtr<FJsonObject> Data);
	void InputFingerDataToAnimInstance(TSharedPtr<FJsonObject> Data);

	void OffsetSensorData(EBodyPart part, FVector& Pos, FQuat& Rot, bool inverse = false);

	void SetShowDeviceModels(bool show);

private:

	void AddSensorDataToJson(TSharedPtr<FJsonObject> JsonObjectFull, const FSensor& Sensor);
	void AddSensorDataToJson(TSharedPtr<FJsonObject> JsonObjectFull, const FSensor& Sensor, const FVector& Pos, const FRotator& Rot);

};
