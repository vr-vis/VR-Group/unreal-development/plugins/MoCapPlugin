// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "CoreMinimal.h"
#include "UObject/Class.h"
#include "Dom/JsonObject.h"
#include "Components/Textblock.h"
#include "MotionControllerComponent.h"

#include "MCDefines.generated.h"

#define IN_DEBUG_MODE false				// Activating this outputs a lot of stuff 

#define MAKE_JSON MakeShared<FJsonObject>()		// Avoid the MakeShared statement all the time

UENUM(BlueprintType)
enum EBodyPart {
	Head = 0 UMETA(DisplayName = "Head"),
	HandL = 1 UMETA(DisplayName = "HandL"),
	HandR = 2 UMETA(DisplayName = "HandR"),
	LowerArmL = 3 UMETA(DisplayName = "LowerArmL"),
	LowerArmR = 4 UMETA(DisplayName = "LowerArmR"),
	LowerLegL = 5 UMETA(DisplayName = "LowerLegL"),
	LowerLegR = 6 UMETA(DisplayName = "LowerLegR"),
	UpperBody = 7 UMETA(DisplayName = "UpperBody"),
	LowerBody = 8 UMETA(DisplayName = "LowerBody"),
	LAST = 9
};

USTRUCT(BlueprintType)
struct FSensor {
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	TEnumAsByte<EBodyPart> BodyPart;

	UPROPERTY(EditAnywhere)
	FName TrackingMotionSource;

	UPROPERTY(BlueprintReadOnly)
	FVector Pos;

	UPROPERTY(BlueprintReadOnly)
	FQuat Rot;

	UPROPERTY(EditAnywhere)
	FName SocketName1;

	UPROPERTY(BlueprintReadOnly)
	UMotionControllerComponent* ControllerComponent;

	UPROPERTY(BlueprintReadOnly)
	UStaticMeshComponent* DebugMesh;

	UPROPERTY(BlueprintReadOnly)
	UTextBlock* StateText;

	FSensor()
		: StateText(nullptr)
	{}

	FSensor(TEnumAsByte<EBodyPart> BodyPart, const FName& TrackingMotionSource, const FName& SocketName1)
		: BodyPart(BodyPart), TrackingMotionSource(TrackingMotionSource), SocketName1(SocketName1), StateText(nullptr)
	{}

};

USTRUCT(BlueprintType)
struct FSensorSetup {
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	FSensor Head;
	UPROPERTY(BlueprintReadOnly)
	FSensor HandL;
	UPROPERTY(BlueprintReadOnly)
	FSensor HandR;

	UPROPERTY(EditAnywhere)
	TArray<FSensor> Trackers;

};

USTRUCT(BlueprintType)
struct FSnapshotAnimations {
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite)
	TArray<FPoseSnapshot> Snapshots;

	TArray<FTimespan> TimeStamps;

	int Fps;
	FTimespan StartTime;
	FTimespan EndTime;

};

USTRUCT(BlueprintType)
struct FSensorDataEntry {
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	bool valid = false;

	UPROPERTY(BlueprintReadOnly)
	bool PosUsed;

	UPROPERTY(BlueprintReadOnly)
	FQuat Rot;

	UPROPERTY(BlueprintReadOnly)
	FVector Pos;

};

USTRUCT(BlueprintType)
struct FSensorData {
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	FSensorDataEntry Head;

	UPROPERTY(BlueprintReadOnly)
	FSensorDataEntry HandL;

	UPROPERTY(BlueprintReadOnly)
	FSensorDataEntry HandR;

	UPROPERTY(BlueprintReadOnly)
	FSensorDataEntry LowerArmL;

	UPROPERTY(BlueprintReadOnly)
	FSensorDataEntry LowerArmR;

	UPROPERTY(BlueprintReadOnly)
	FSensorDataEntry LowerLegL;

	UPROPERTY(BlueprintReadOnly)
	FSensorDataEntry LowerLegR;

	UPROPERTY(BlueprintReadOnly)
	FSensorDataEntry UpperBody;

	UPROPERTY(BlueprintReadOnly)
	FSensorDataEntry LowerBody;

	FSensorDataEntry* GetEntry(EBodyPart BodyPart);

};

USTRUCT(BlueprintType)
struct FFingerDataEntry {
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	float Thumb;

	UPROPERTY(BlueprintReadOnly)
	float Index;

	UPROPERTY(BlueprintReadOnly)
	float Middle;

	UPROPERTY(BlueprintReadOnly)
	float Ring;

	UPROPERTY(BlueprintReadOnly)
	float Pinky;

	UPROPERTY(BlueprintReadOnly)
	float Thumb_Index;

	UPROPERTY(BlueprintReadOnly)
	float Index_Middle;

	UPROPERTY(BlueprintReadOnly)
	float Middle_Ring;

	UPROPERTY(BlueprintReadOnly)
	float Ring_Pinky;

};

USTRUCT(BlueprintType)
struct FFingerData {
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	FFingerDataEntry LeftHand;

	UPROPERTY(BlueprintReadOnly)
	FFingerDataEntry RightHand;

};

USTRUCT(BlueprintType)
struct FSensorOffset {
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	FTransform DeltaTransform;

};

USTRUCT(BlueprintType)
struct FSensorOffsets {
	GENERATED_BODY()
	
	UPROPERTY(BlueprintReadOnly)
	FSensorOffset Head;

	UPROPERTY(BlueprintReadOnly)
	FSensorOffset HandL;

	UPROPERTY(BlueprintReadOnly)
	FSensorOffset HandR;

	UPROPERTY(BlueprintReadOnly)
	FSensorOffset LowerLegL;

	UPROPERTY(BlueprintReadOnly)
	FSensorOffset LowerLegR;

	UPROPERTY(BlueprintReadOnly)
	FSensorOffset LowerArmL;

	UPROPERTY(BlueprintReadOnly)
	FSensorOffset LowerArmR;

	UPROPERTY(BlueprintReadOnly)
	FSensorOffset LowerBody;

	UPROPERTY(BlueprintReadOnly)
	FSensorOffset UpperBody;

};

USTRUCT(BlueprintType)
struct FAdditionalOffsets {
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	FRotator HeadRotOffset = FRotator::ZeroRotator;

	UPROPERTY(EditAnywhere)
	FRotator HandLRotOffset = FRotator::ZeroRotator;

	UPROPERTY(EditAnywhere)
	FRotator HandRRotOffset = FRotator::ZeroRotator;

	UPROPERTY(EditAnywhere)
	FRotator LowerLegLRotOffset = FRotator::ZeroRotator;

	UPROPERTY(EditAnywhere)
	FRotator LowerLegRRotOffset = FRotator::ZeroRotator;

	UPROPERTY(EditAnywhere)
	FRotator LowerArmLRotOffset = FRotator::ZeroRotator;

	UPROPERTY(EditAnywhere)
	FRotator LowerArmRRotOffset = FRotator::ZeroRotator;

	UPROPERTY(EditAnywhere)
	FRotator LowerBodyRotOffset = FRotator::ZeroRotator;

	UPROPERTY(EditAnywhere)
	FRotator UpperBodyRotOffset = FRotator::ZeroRotator;

	UPROPERTY(EditAnywhere)
	FVector HeadPosOffset = FVector::ZeroVector;

	UPROPERTY(EditAnywhere)
	FVector HandLPosOffset = FVector::ZeroVector;

	UPROPERTY(EditAnywhere)
	FVector HandRPosOffset = FVector::ZeroVector;

	UPROPERTY(EditAnywhere)
	FVector LowerLegLPosOffset = FVector::ZeroVector;

	UPROPERTY(EditAnywhere)
	FVector LowerLegRPosOffset = FVector::ZeroVector;

	UPROPERTY(EditAnywhere)
	FVector LowerArmLPosOffset = FVector::ZeroVector;

	UPROPERTY(EditAnywhere)
	FVector LowerArmRPosOffset = FVector::ZeroVector;

	UPROPERTY(EditAnywhere)
	FVector LowerBodyPosOffset = FVector::ZeroVector;

	UPROPERTY(EditAnywhere)
	FVector UpperBodyPosOffset = FVector::ZeroVector;

};

struct FMCAppliedLength {
	float applied = 1;
	float original = 1;
	FName endBone;
	FCompactPoseBoneIndex index;

	FMCAppliedLength() : index(0) {};
};

USTRUCT(BlueprintType)
struct FBodyProportionStruct {
	GENERATED_BODY()

public:

	//Distance from shoulder to shoulder
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ShoulderToShoulder = 35.0f;
	//Distance from middle of shoulder joint to elbow
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float UpperArm = 27.5f;
	//Distance from elbow to wrist
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float LowerArm = 26.0f;

	//Distance from floor to hip
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float HipHeight = 102.0f;
	//Height from Hipbones to top of shoulders
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float NeckToHip = 50.5f;
	//Height from top of shoulders to slidely under the mouth
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float HeadToNeck = 17.0f;

	//Distance at the hip between the middle of legs
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float HipWidth = 26.0f;
	//Distance from knee to middle of leg at hip
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float UpperLeg = 43.5f;
	//Distance from middle between sole and ankle to knee
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float LowerLeg = 47.0f;

	UPROPERTY(BlueprintReadWrite)
	bool calcedAppliedLengths = false;
	TArray<FMCAppliedLength> appliedLengths;

	UPROPERTY(BlueprintReadWrite)
	bool loaded = false;

};

USTRUCT(BlueprintType)
struct FFingerAngles {
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector Thumb;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector Index;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector Middle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector Ring;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector Pinky;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Thumb_Index;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Index_Middle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Middle_Ring;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Ring_Pinky;

};

struct FProcessedAnimData {
	bool IsMarker = false;
	bool IsEnd = false;
	FTimespan Timestamp;
	FFingerData FingerData;
	FSensorData SensorData;
};

enum LinearTransformType {
	SmoothStart,
	SmoothStop
};

class AMCPawn;

USTRUCT()
struct FAnimSaveState {
	GENERATED_BODY()

	TArray<FString> StringData;
	TArray<FProcessedAnimData> AnimData;
	int CurrentEntryIndex;
	int CurrentMarker;
	FTimespan LastMarker;
	FTimespan NextFrame;
	int FPS;
	float SPF;

	bool WaitForAnimInstance;

	UPROPERTY()
	AMCPawn* Pawn;

	UPROPERTY()
	TArray<UAnimSequence*> AnimSequences;

};