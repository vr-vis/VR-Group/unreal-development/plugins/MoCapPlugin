// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BoneControllers/AnimNode_SkeletalControlBase.h"
#include "VirtualHuman.h"
#include "MCDefines.h"
#include "MCPawn.h"
#include "KismetAnimationLibrary.h"
#include "MCCalibrationNode.generated.h"

USTRUCT(BlueprintType)
struct MOCAPPLUGIN_API FMCCalibrationNode : public FAnimNode_SkeletalControlBase
{
	GENERATED_BODY()
	
    TArray<FBoneReference> BonesToModify;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links, meta = (PinShownByDefault))
    AMCPawn* pawn;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links, meta = (PinShownByDefault))
    bool reverse;

    virtual void EvaluateSkeletalControl_AnyThread(FComponentSpacePoseContext& Output, TArray<FBoneTransform>& OutBoneTransforms) override;
    virtual bool IsValidToEvaluate(const USkeleton* Skeleton, const FBoneContainer& RequiredBones) override;
    virtual void GatherDebugData(FNodeDebugData& DebugData) override;

    FMCCalibrationNode();

private:

    bool SetUpSkeletonBones();
    void CalcAppliedLengths(const FBoneContainer& RequiredBones, FBodyProportionStruct& measurements);
    void AddAppliedLength(const FName& startBone, const FName& endBone, float appliedLength, FBodyProportionStruct& measurements, const USkeletalMeshComponent* skeletalMesh, const FBoneContainer& RequiredBones);
    float GetOriginalDistance(const USkeletalMeshComponent* skeletalMesh, const FName& startBone, const FName& endBone);

    virtual void InitializeBoneReferences(const FBoneContainer& RequiredBones) override;
};
