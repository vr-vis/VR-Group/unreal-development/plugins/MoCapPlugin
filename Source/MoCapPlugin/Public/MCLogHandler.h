// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "MCDefines.h"
#include "MCUtils.h"
#include "HAL/FileManagerGeneric.h"

//#include "MCLogHandler.generated.h"

class UMCLogHandler {

public:

	FString CurLog;

	FString GetSessionIdentifier();
	void NewLog(const FString& Name);
    void LogData(const FString& Message);

	void WriteToLogFile();
	void CopyLogToRecordings(FString& Name);

	void SetMarker();
	void StartRecording();
	void StopRecording();
	

protected:

	TArray<FString> DataLogArray;

};

