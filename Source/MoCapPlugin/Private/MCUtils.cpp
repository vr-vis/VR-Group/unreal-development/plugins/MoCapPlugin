// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "MCUtils.h"

FString MCUtils::JsonToString(TSharedPtr<FJsonObject> Json, bool bFormat) {
	FString OutputString;
	TSharedRef<TJsonWriter<>> Writer = TJsonWriterFactory<>::Create(&OutputString);
	FJsonSerializer::Serialize(Json.ToSharedRef(), Writer);

	if (bFormat) {
		OutputString = OutputString.Replace(TEXT("\n"), TEXT(""));
		OutputString = OutputString.Replace(TEXT("\t"), TEXT(""));
		OutputString = OutputString.Replace(TEXT("\r"), TEXT(""));
		OutputString = OutputString.Replace(TEXT(" "), TEXT(""));
	}

	return OutputString;
}

TSharedPtr<FJsonObject> MCUtils::StringToJson(FString String) {
	TSharedPtr<FJsonObject> Json = MakeShareable(new FJsonObject());
	TSharedRef<TJsonReader<TCHAR>> Reader = FJsonStringReader::Create(String);
	FJsonSerializer::Deserialize(Reader, Json);

	return Json;
}

FTimespan MCUtils::StringToTimespan(FString TimeString) {
	FString Tmp, Min, Sec, Mil;

	TimeString.Split(":", &Min, &Tmp);
	Tmp.Split(":", &Sec, &Mil);


	const int IntMin = FCString::Atoi(*Min);
	const int IntSec = FCString::Atoi(*Sec);
	const int IntMil = FCString::Atoi(*Mil);

	const FTimespan TimespanMil = FTimespan::FromMilliseconds(IntMil);
	const FTimespan TimespanSec = FTimespan::FromSeconds(IntSec);
	const FTimespan TimespanMin = FTimespan::FromMinutes(IntMin);

	const FTimespan TimespanTotal = TimespanMil + TimespanMin + TimespanSec;

	return TimespanTotal;
}
