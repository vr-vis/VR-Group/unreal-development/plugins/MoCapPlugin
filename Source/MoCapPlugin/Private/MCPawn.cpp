// Fill out your copyright notice in the Description page of Project Settings.


#include "MCPawn.h"

#include "DrawDebugHelpers.h"

AMCPawn::AMCPawn(const FObjectInitializer& ObjectInitializer)
	: APawn(ObjectInitializer)
{

	PrimaryActorTick.bCanEverTick = true;

	AutoPossessPlayer = EAutoReceiveInput::Player0; // Necessary for receiving motion controller events.

	SpawnCollisionHandlingMethod = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	SetBoneNames();

	//----init components----

	SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMesh"));
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> ActorMesh(TEXT("/CharacterPlugin/Characters/Henry/Henry.Henry"));
	SkeletalMesh->SetSkeletalMesh(ActorMesh.Object);
	static ConstructorHelpers::FClassFinder<UMCAnimInstance> SaveAnimInstance(TEXT("/MoCapPlugin/SaveSequenceAnimBP"));
	SkeletalMesh->SetAnimInstanceClass(SaveAnimInstance.Class);
	SkeletalMesh->SetVisibility(true, true);
	SkeletalMesh->SetupAttachment(RootComponent);

	
	SensorSetup.Trackers.Add(FSensor(EBodyPart::LowerLegL, "Tracker_Foot_Left", BoneNames.foot_l));
	SensorSetup.Trackers.Add(FSensor(EBodyPart::LowerLegR, "Tracker_Foot_Right", BoneNames.foot_r));
	SensorSetup.Trackers.Add(FSensor(EBodyPart::LowerArmL, "Tracker_Elbow_Left", BoneNames.lowerarm_l));
	SensorSetup.Trackers.Add(FSensor(EBodyPart::LowerArmR, "Tracker_Elbow_Right", BoneNames.lowerarm_r));
	SensorSetup.Trackers.Add(FSensor(EBodyPart::UpperBody, "Tracker_Chest", BoneNames.spine03));
	SensorSetup.Trackers.Add(FSensor(EBodyPart::LowerBody, "Tracker_Waist", BoneNames.pelvis));

	VROrigin = CreateDefaultSubobject<USceneComponent>(TEXT("VROrigin"));
	VROrigin->SetupAttachment(RootComponent);
	VROrigin->SetRelativeLocation(FVector(0, 0, 0));
	//VROrigin->SetRelativeLocation(FVector(0, 0, 48));

	VRCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("VRCamera"));
	VRCamera->SetupAttachment(VROrigin);

	SensorSetup.Head.SocketName1 = BoneNames.head;

	SensorSetup.HandL.ControllerComponent = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("MC_LeftHand"));
	SensorSetup.HandL.ControllerComponent->SetupAttachment(VROrigin);
	SensorSetup.HandL.ControllerComponent->SetTrackingSource(EControllerHand::Left);
	SensorSetup.HandL.SocketName1 = BoneNames.hand_l;
	SensorSetup.HandL.BodyPart = EBodyPart::HandL;

	SensorSetup.HandR.ControllerComponent = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("MC_RightHand"));
	SensorSetup.HandR.ControllerComponent->SetupAttachment(VROrigin);
	SensorSetup.HandR.ControllerComponent->SetTrackingSource(EControllerHand::Right);
	SensorSetup.HandR.SocketName1 = BoneNames.hand_r;
	SensorSetup.HandR.BodyPart = EBodyPart::HandR;

	for (FSensor& Tracker : SensorSetup.Trackers) {
		Tracker.ControllerComponent = CreateDefaultSubobject<UMotionControllerComponent>(Tracker.TrackingMotionSource);
		Tracker.ControllerComponent->SetupAttachment(VROrigin);
		Tracker.ControllerComponent->SetTrackingMotionSource(Tracker.TrackingMotionSource);

		FText Delimiter = FText::FromString(TEXT(""));
		TArray<FText> Arr = { FText::FromString(TEXT("DebugMesh_")), FText::FromName(Tracker.TrackingMotionSource) };
		FText Name = FText::Join(Delimiter, Arr);
		Tracker.DebugMesh = CreateDefaultSubobject<UStaticMeshComponent>(*Name.ToString());
		Tracker.DebugMesh->SetupAttachment(Tracker.ControllerComponent);
		static ConstructorHelpers::FObjectFinder<UStaticMesh> sphereMesh2(TEXT("/Engine/BasicShapes/Sphere"));
		if (sphereMesh2.Succeeded()) {
			Tracker.DebugMesh->SetStaticMesh(sphereMesh2.Object);
		}
		Tracker.DebugMesh->SetRelativeScale3D(FVector(0.0625f, 0.0625f, 0.0625f));
		Tracker.DebugMesh->SetVisibility(false);
	}



}

void AMCPawn::BeginPlay() {
	Super::BeginPlay();

	SetShowDeviceModels(ShowDeviceModels);

}

void AMCPawn::Tick(float DeltaSeconds) {
	Super::Tick(DeltaSeconds);

	bool show = false;
	if (show) {
		for (FSensor& Tracker : SensorSetup.Trackers) {
			DrawDebugSphere(GetWorld(), Tracker.ControllerComponent->GetComponentLocation(), 5, 5, FColor::Red, false, 1);
		}
		DrawDebugSphere(GetWorld(), SensorSetup.HandL.ControllerComponent->GetComponentLocation(), 5, 5, FColor::Red, false, 1);
		DrawDebugSphere(GetWorld(), SensorSetup.HandR.ControllerComponent->GetComponentLocation(), 5, 5, FColor::Red, false, 1);
	}

}

void AMCPawn::SetBoneNames() {
	BoneNames.boneroot = TEXT("root");
	BoneNames.spine01 = TEXT("spine_01");
	BoneNames.spine02 = TEXT("spine_02");
	BoneNames.spine03 = TEXT("spine_03");
	BoneNames.spine04 = TEXT("");
	BoneNames.spine05 = TEXT("");

	BoneNames.neck = TEXT("neck_01");
	BoneNames.neck02 = TEXT("");
	BoneNames.head = TEXT("head");
	BoneNames.eye_r = TEXT("cc_base_r_eye");
	BoneNames.eye_l = TEXT("cc_base_l_eye");
	BoneNames.jaw = TEXT("cc_base_jawroot");
	BoneNames.teeth_lower = TEXT("cc_base_jawroot");
	BoneNames.mouth_lower = TEXT("cc_base_jawroot");

	BoneNames.clavicle_l = TEXT("clavicle_l");
	BoneNames.clavicle_r = TEXT("clavicle_r");
	BoneNames.upperarm_l = TEXT("upperarm_l");
	BoneNames.upperarm_r = TEXT("upperarm_r");
	BoneNames.lowerarm_l = TEXT("lowerarm_l");
	BoneNames.lowerarm_r = TEXT("lowerarm_r");
	BoneNames.hand_l = TEXT("hand_l");
	BoneNames.hand_r = TEXT("hand_r");

	BoneNames.pelvis = TEXT("pelvis");
	BoneNames.calf_l = TEXT("calf_l");
	BoneNames.calf_r = TEXT("calf_r");
	BoneNames.thigh_l = TEXT("thigh_l");
	BoneNames.thigh_r = TEXT("thigh_r");
	BoneNames.foot_r = TEXT("foot_r");
	BoneNames.foot_l = TEXT("foot_l");
	BoneNames.foot_socket_r = TEXT("foot_rSocket");
	BoneNames.foot_socket_l = TEXT("foot_lSocket");
}

UMCAnimInstance* AMCPawn::GetAnimInstance() {
	return Cast<UMCAnimInstance>(SkeletalMesh->GetAnimInstance());
}

USkeleton* AMCPawn::GetSkeleton() {
	return SkeletalMesh->SkeletalMesh->GetSkeleton();
}

void AMCPawn::SetMeshVisibility(bool visible) {
	SkeletalMesh->SetVisibility(visible, true);
}

void AMCPawn::LoadMeasurementFile(const FString& Filepath) {

	UMCAnimInstance* AI = GetAnimInstance();

	FString FileAsString;
	FString Filename = FPaths::Combine(FPaths::ProjectDir(), Filepath);
	IPlatformFile& File = FPlatformFileManager::Get().GetPlatformFile();
	if (File.FileExists(*Filename)) {
		FFileHelper::LoadFileToString(FileAsString, *Filename);
	}
	else {
		return;
	}

	TArray<FString> Lines;
	FileAsString.ParseIntoArrayLines(Lines, true);

	for (const FString& Line : Lines) {
		if (!Line.StartsWith("//") && !Line.IsEmpty()) {

			int32 index = Line.Find("=");
			FString MeasurementType = Line.Left(index);
			MeasurementType.RemoveSpacesInline();
			FString Value = Line.RightChop(index + 1);
			Value.RemoveSpacesInline();
			float FloatValue = FCString::Atof(*Value);

			if (MeasurementType == "ShoulderToShoulder") {
				AI->Measurements.ShoulderToShoulder = FloatValue;
			}
			else if (MeasurementType == "UpperArm") {
				AI->Measurements.UpperArm = FloatValue;
			}
			else if (MeasurementType == "LowerArm") {
				AI->Measurements.LowerArm = FloatValue;
			}
			else if (MeasurementType == "HipHeight") {
				AI->Measurements.HipHeight = FloatValue;
			}
			else if (MeasurementType == "NeckToHip") {
				AI->Measurements.NeckToHip = FloatValue;
			}
			else if (MeasurementType == "HeadToNeck") {
				AI->Measurements.HeadToNeck = FloatValue;
			}
			else if (MeasurementType == "HipWidth") {
				AI->Measurements.HipWidth = FloatValue;
			}
			else if (MeasurementType == "UpperLeg") {
				AI->Measurements.UpperLeg = FloatValue;
			}
			else if (MeasurementType == "LowerLeg") {
				AI->Measurements.LowerLeg = FloatValue;
			}

		}
	}

	AI->Measurements.loaded = true;

}

void AMCPawn::CalcSensorOffsets(UMCLogHandler& LogHandler, bool UseLastOffsets, bool DebugMode) {

	TArray<FSensor> Sensors = SensorSetup.Trackers;
	Sensors.Insert(SensorSetup.HandR, 0);
	Sensors.Insert(SensorSetup.HandL, 0);
	Sensors.Insert(SensorSetup.Head, 0);	

	TSharedPtr<FJsonObject> Json = MAKE_JSON;
	Json->SetStringField("Type", "Offsets");

	FSensorOffset* Offset = nullptr;

	for (FSensor& Tracker : Sensors) {

		switch (Tracker.BodyPart) {
		case EBodyPart::LowerLegL:
			Offset = &LastOffsets.LowerLegL;
			break;
		case EBodyPart::LowerLegR:
			Offset = &LastOffsets.LowerLegR;
			break;
		case EBodyPart::HandL:
			Offset = &LastOffsets.HandL;
			break;
		case EBodyPart::HandR:
			Offset = &LastOffsets.HandR;
			break;
		case EBodyPart::LowerArmL:
			Offset = &LastOffsets.LowerArmL;
			break;
		case EBodyPart::LowerArmR:
			Offset = &LastOffsets.LowerArmR;
			break;
		case EBodyPart::LowerBody:
			Offset = &LastOffsets.LowerBody;
			break;
		case EBodyPart::UpperBody:
			Offset = &LastOffsets.UpperBody;
			break;
		case EBodyPart::Head:
			Offset = &LastOffsets.Head;
			break;
		default:
			Offset = &LastOffsets.Head;
			break;
		}

		TSharedPtr<FJsonObject> JsonTracker = MAKE_JSON;

		FVector Loc;
		FQuat Rot;
		FQuat RotInv;
		FName SocketName1;
		if (Tracker.BodyPart == EBodyPart::Head) {
			Loc = VRCamera->GetComponentLocation();
			Rot = VRCamera->GetComponentQuat();
			RotInv = Rot.Inverse();
			SocketName1 = BoneNames.head;
		}
		else {
			Loc = Tracker.ControllerComponent->GetComponentLocation();
			Rot = Tracker.ControllerComponent->GetComponentQuat();
			RotInv = Rot.Inverse();
			SocketName1 = Tracker.SocketName1;
		}
		//TODO ?? Does it? Test! it matters where the pawn is located in the level for this offset capture! Should not be this way, but is it acceptable?
		if (Tracker.SocketName1 != "") {
			if (!UseLastOffsets) {

				FTransform DataTransform(Rot, Loc);
				FTransform BoneTransform(SkeletalMesh->GetSocketQuaternion(SocketName1), SkeletalMesh->GetSocketLocation(SocketName1));
				FTransform DeltaTransform = BoneTransform.GetRelativeTransform(DataTransform);

				FVector DeltaPos = DeltaTransform.GetTranslation();
				FQuat DeltaRot = DeltaTransform.GetRotation();

				JsonTracker->SetStringField("PosX", FString::SanitizeFloat(DeltaPos.X));
				JsonTracker->SetStringField("PosY", FString::SanitizeFloat(DeltaPos.Y));
				JsonTracker->SetStringField("PosZ", FString::SanitizeFloat(DeltaPos.Z));

				JsonTracker->SetStringField("RotX", FString::SanitizeFloat(DeltaRot.X));
				JsonTracker->SetStringField("RotY", FString::SanitizeFloat(DeltaRot.Y));
				JsonTracker->SetStringField("RotZ", FString::SanitizeFloat(DeltaRot.Z));
				JsonTracker->SetStringField("RotW", FString::SanitizeFloat(DeltaRot.W));

				Offset->DeltaTransform = DeltaTransform;

			}
			else {

				FVector DeltaPos = Offset->DeltaTransform.GetTranslation();
				FQuat DeltaRot = Offset->DeltaTransform.GetRotation();

				JsonTracker->SetStringField("PosX", FString::SanitizeFloat(DeltaPos.X));
				JsonTracker->SetStringField("PosY", FString::SanitizeFloat(DeltaPos.Y));
				JsonTracker->SetStringField("PosZ", FString::SanitizeFloat(DeltaPos.Z));

				JsonTracker->SetStringField("RotX", FString::SanitizeFloat(DeltaRot.X));
				JsonTracker->SetStringField("RotY", FString::SanitizeFloat(DeltaRot.Y));
				JsonTracker->SetStringField("RotZ", FString::SanitizeFloat(DeltaRot.Z));
				JsonTracker->SetStringField("RotW", FString::SanitizeFloat(DeltaRot.W));
				
			}
		}

		Json->SetObjectField(UEnum::GetDisplayValueAsText(Tracker.BodyPart).ToString(), JsonTracker);

	}

	LogHandler.LogData(MCUtils::JsonToString(Json));

	if (DebugMode) {
		InputViveOffsetsToAnimInstance(Json);
	}

}

void AMCPawn::AddSensorDataToJson(TSharedPtr<FJsonObject> JsonObjectFull) {

	AddSensorDataToJson(JsonObjectFull, SensorSetup.Head, VRCamera->GetRelativeLocation(), VRCamera->GetRelativeRotation());
	AddSensorDataToJson(JsonObjectFull, SensorSetup.HandL);
	AddSensorDataToJson(JsonObjectFull, SensorSetup.HandR);

	for (const FSensor& Tracker : SensorSetup.Trackers) {
		AddSensorDataToJson(JsonObjectFull, Tracker);
	}

}

void AMCPawn::AddFingerDataToJson(TSharedPtr<FJsonObject> JsonObjectFull, ESkeletalSummaryDataType FingerTrackingMethod, UMCFeedbackWidget* FeedbackWidget) {

	EHand VRHand = EHand::VR_LeftHand;

	for (int i = 0; i < 2; i++) {

		FSteamVRFingerCurls Curls;
		FSteamVRFingerSplays Splays;
		USteamVRInputDeviceFunctionLibrary::GetFingerCurlsAndSplays(VRHand, Curls, Splays, FingerTrackingMethod);

		TSharedPtr<FJsonObject> JsonObject = MAKE_JSON;

		JsonObject->SetStringField("Thumb", FString::SanitizeFloat(Curls.Thumb));
		JsonObject->SetStringField("Index", FString::SanitizeFloat(Curls.Index));
		JsonObject->SetStringField("Middle", FString::SanitizeFloat(Curls.Middle));
		JsonObject->SetStringField("Pinky", FString::SanitizeFloat(Curls.Pinky));
		JsonObject->SetStringField("Ring", FString::SanitizeFloat(Curls.Ring));

		JsonObject->SetStringField("Thumb_Index", FString::SanitizeFloat(Splays.Thumb_Index));
		JsonObject->SetStringField("Index_Middle", FString::SanitizeFloat(Splays.Index_Middle));
		JsonObject->SetStringField("Middle_Ring", FString::SanitizeFloat(Splays.Middle_Ring));
		JsonObject->SetStringField("Ring_Pinky", FString::SanitizeFloat(Splays.Ring_Pinky));

		if (i == 0) {
			JsonObjectFull->SetObjectField("CurlsAndSplaysLeft", JsonObject);
			VRHand = EHand::VR_RightHand;
		}
		else {
			JsonObjectFull->SetObjectField("CurlsAndSplaysRight", JsonObject);
		}

		if (FeedbackWidget && FeedbackWidget->LeftFingerText && FeedbackWidget->RightFingerText) {
			FString Side = (i == 0 ? "Left Fingers" : "Right Fingers");
			FString str = Side + ": \tCurls(" + FString::Printf(TEXT("%.1f"), Curls.Thumb) + ", " + FString::Printf(TEXT("%.1f"), Curls.Index) + ", " + FString::Printf(TEXT("%.1f"), Curls.Middle) + ", " + FString::Printf(TEXT("%.1f"), Curls.Ring) + ", " + FString::Printf(TEXT("%.1f"), Curls.Pinky)
				+ ") \t Splays(" + FString::Printf(TEXT("%.1f"), Splays.Thumb_Index) + ", " + FString::Printf(TEXT("%.1f"), Splays.Index_Middle) + ", " + FString::Printf(TEXT("%.1f"), Splays.Middle_Ring) + ", " + FString::Printf(TEXT("%.1f"), Splays.Ring_Pinky) + ") (Thumb->Pinky)";
			if (i == 0) {
				FeedbackWidget->LeftFingerText->SetText(FText::FromString(str));
			}
			else {
				FeedbackWidget->RightFingerText->SetText(FText::FromString(str));
			}
		}

	}

}

void AMCPawn::AICalcFrame() {

	UMCAnimInstance* AI = GetAnimInstance();
	AI->AppliedPose = false;

}

void AMCPawn::InputViveDataToAnimInstance(TSharedPtr<FJsonObject> Data) {

	UMCAnimInstance* AI = GetAnimInstance();

	for (int i = 0; i < EBodyPart::LAST; i++) {

		FString Type = UEnum::GetDisplayValueAsText(EBodyPart(i)).ToString();
		if (Data->HasField(Type)) {

			TSharedPtr<FJsonObject> JsonObj = Data->GetObjectField(Type);

			const float Roll = FCString::Atof(*JsonObj->GetStringField("Roll"));
			const float Pitch = FCString::Atof(*JsonObj->GetStringField("Pitch"));
			const float Yaw = FCString::Atof(*JsonObj->GetStringField("Yaw"));
			FRotator Rot(Pitch, Yaw, Roll);

			const float PosX = FCString::Atof(*JsonObj->GetStringField("PosX"));
			const float PosY = FCString::Atof(*JsonObj->GetStringField("PosY"));
			const float PosZ = FCString::Atof(*JsonObj->GetStringField("PosZ"));
			FVector Pos(PosX, PosY, PosZ);

			AI->SetSensorData((EBodyPart)i, Pos, Rot.Quaternion());
		}
	}

}

void AMCPawn::InputViveOffsetsToAnimInstance(TSharedPtr<FJsonObject> Data) {

	UMCAnimInstance* AI = GetAnimInstance();

	for (int i = 0; i < EBodyPart::LAST; i++) {

		EBodyPart BodyPart = (EBodyPart)i;
		FString Type = UEnum::GetDisplayValueAsText(EBodyPart(i)).ToString();
		if (Data->HasField(Type)) {

			TSharedPtr<FJsonObject> JsonObj = Data->GetObjectField(Type);

			if (JsonObj->HasField("RotX")) {

				const float RotX = FCString::Atof(*JsonObj->GetStringField("RotX"));
				const float RotY = FCString::Atof(*JsonObj->GetStringField("RotY"));
				const float RotZ = FCString::Atof(*JsonObj->GetStringField("RotZ"));
				const float RotW = FCString::Atof(*JsonObj->GetStringField("RotW"));
				FQuat Rot(RotX, RotY, RotZ, RotW);

				const float PosX = FCString::Atof(*JsonObj->GetStringField("PosX"));
				const float PosY = FCString::Atof(*JsonObj->GetStringField("PosY"));
				const float PosZ = FCString::Atof(*JsonObj->GetStringField("PosZ"));
				FVector Pos(PosX, PosY, PosZ);

				AI->SetSensorOffset(BodyPart, FTransform(Rot, Pos));
			
			}

		}
	}

}

void AMCPawn::InputFingerDataToAnimInstance(TSharedPtr<FJsonObject> Data) {

	UMCAnimInstance* AI = GetAnimInstance();

	if (!Data->HasField("CurlsAndSplaysLeft")) {
		return;
	}

	for (int i = 0; i < 2; i++) {

		TSharedPtr<FJsonObject> JsonObj = Data->GetObjectField(i == 0 ? "CurlsAndSplaysLeft" : "CurlsAndSplaysRight");
		FFingerDataEntry* Entry = (i == 0 ? &AI->FingerData.LeftHand : &AI->FingerData.RightHand);

		Entry->Thumb = FCString::Atof(*JsonObj->GetStringField("Thumb"));
		Entry->Index = FCString::Atof(*JsonObj->GetStringField("Index"));
		Entry->Middle = FCString::Atof(*JsonObj->GetStringField("Middle"));
		Entry->Ring = FCString::Atof(*JsonObj->GetStringField("Ring"));
		Entry->Pinky = FCString::Atof(*JsonObj->GetStringField("Pinky"));

		Entry->Thumb_Index = FCString::Atof(*JsonObj->GetStringField("Thumb_Index"));
		Entry->Index_Middle = FCString::Atof(*JsonObj->GetStringField("Index_Middle"));
		Entry->Middle_Ring = FCString::Atof(*JsonObj->GetStringField("Middle_Ring"));
		Entry->Ring_Pinky = FCString::Atof(*JsonObj->GetStringField("Ring_Pinky"));

	}

}

void AMCPawn::OffsetSensorData(EBodyPart part, FVector & Pos, FQuat & Rot, bool inverse) {

	FSensorOffset* Offset = nullptr;
	UMCAnimInstance* AI = GetAnimInstance();
	
	switch (part) {
	case EBodyPart::Head:
		Offset = &AI->SensorOffsets.Head;
		break;
	case EBodyPart::HandL:
		Offset = &AI->SensorOffsets.HandL;
		break;
	case EBodyPart::HandR:
		Offset = &AI->SensorOffsets.HandR;
		break;
	case EBodyPart::LowerLegL:
		Offset = &AI->SensorOffsets.LowerLegL;
		break;
	case EBodyPart::LowerLegR:
		Offset = &AI->SensorOffsets.LowerLegR;
		break;
	case EBodyPart::LowerArmL:
		Offset = &AI->SensorOffsets.LowerArmL;
		break;
	case EBodyPart::LowerArmR:
		Offset = &AI->SensorOffsets.LowerArmR;
		break;
	case EBodyPart::LowerBody:
		Offset = &AI->SensorOffsets.LowerBody;
		break;
	case EBodyPart::UpperBody:
		Offset = &AI->SensorOffsets.UpperBody;
		break;
	}

	FTransform OffsetTrans = Offset->DeltaTransform;
	if (inverse) {
		OffsetTrans = OffsetTrans.Inverse();
	}

	FTransform Transform(Rot, Pos);
	Transform = OffsetTrans * Transform;

	Rot = Transform.GetRotation();
	Pos = Transform.GetTranslation();

}

void AMCPawn::SetShowDeviceModels(bool show) {
	ShowDeviceModels = show;
	SensorSetup.HandL.ControllerComponent->SetShowDeviceModel(show);
	SensorSetup.HandR.ControllerComponent->SetShowDeviceModel(show);
	for (FSensor& Tracker : SensorSetup.Trackers) {
		Tracker.ControllerComponent->SetShowDeviceModel(show);
		Tracker.DebugMesh->SetVisibility(show);
	}
}

void AMCPawn::AddSensorDataToJson(TSharedPtr<FJsonObject> JsonObjectFull, const FSensor& Sensor) {
	TSharedPtr<FJsonObject> JsonObject = MAKE_JSON;

	const FRotator Rot = Sensor.ControllerComponent->GetRelativeRotation();
	JsonObject->SetStringField("Roll", FString::SanitizeFloat(Rot.Roll));
	JsonObject->SetStringField("Pitch", FString::SanitizeFloat(Rot.Pitch));
	JsonObject->SetStringField("Yaw", FString::SanitizeFloat(Rot.Yaw));

	const FVector Loc = Sensor.ControllerComponent->GetRelativeLocation();
	JsonObject->SetStringField("PosX", FString::SanitizeFloat(Loc.X));
	JsonObject->SetStringField("PosY", FString::SanitizeFloat(Loc.Y));
	JsonObject->SetStringField("PosZ", FString::SanitizeFloat(Loc.Z));

	JsonObjectFull->SetObjectField(UEnum::GetDisplayValueAsText(Sensor.BodyPart).ToString(), JsonObject);

	if (Sensor.StateText) {
		FString str = UEnum::GetDisplayValueAsText(Sensor.BodyPart).ToString()
			+ ": \tPos(" + FString::Printf(TEXT("%.1f"), Loc.X) + ", " + FString::Printf(TEXT("%.1f"), Loc.Y) + ", " + FString::Printf(TEXT("%.1f"), Loc.Z) + ")"
			+ " \tRot(" + FString::Printf(TEXT("%.1f"), Rot.Pitch) + ", " + FString::Printf(TEXT("%.1f"), Rot.Yaw) + ", " + FString::Printf(TEXT("%.1f"), Rot.Roll) + ")";
		Sensor.StateText->SetText(FText::FromString(str));
		if (Loc.Equals(FVector::ZeroVector)) {
			Sensor.StateText->SetColorAndOpacity(FSlateColor(FLinearColor::Red));
		}
		else {
			Sensor.StateText->SetColorAndOpacity(FSlateColor(FLinearColor::White));
		}
	}
}

void AMCPawn::AddSensorDataToJson(TSharedPtr<FJsonObject> JsonObjectFull, const FSensor& Sensor, const FVector& Pos, const FRotator& Rot) {
	TSharedPtr<FJsonObject> JsonObject = MAKE_JSON;

	JsonObject->SetStringField("Roll", FString::SanitizeFloat(Rot.Roll));
	JsonObject->SetStringField("Pitch", FString::SanitizeFloat(Rot.Pitch));
	JsonObject->SetStringField("Yaw", FString::SanitizeFloat(Rot.Yaw));

	JsonObject->SetStringField("PosX", FString::SanitizeFloat(Pos.X));
	JsonObject->SetStringField("PosY", FString::SanitizeFloat(Pos.Y));
	JsonObject->SetStringField("PosZ", FString::SanitizeFloat(Pos.Z));

	JsonObjectFull->SetObjectField(UEnum::GetDisplayValueAsText(Sensor.BodyPart).ToString(), JsonObject);

	if (Sensor.StateText) {
		FString str = UEnum::GetDisplayValueAsText(Sensor.BodyPart).ToString()
			+ ": \tPos(" + FString::Printf(TEXT("%.1f"), Pos.X) + ", " + FString::Printf(TEXT("%.1f"), Pos.Y) + ", " + FString::Printf(TEXT("%.1f"), Pos.Z) + ")"
			+ " \tRot(" + FString::Printf(TEXT("%.1f"), Rot.Pitch) + ", " + FString::Printf(TEXT("%.1f"), Rot.Yaw) + ", " + FString::Printf(TEXT("%.1f"), Rot.Roll) + ")";
		Sensor.StateText->SetText(FText::FromString(str));
		if (Pos.Equals(FVector::ZeroVector)) {
			Sensor.StateText->SetColorAndOpacity(FSlateColor(FLinearColor::Red));
		}
		else {
			Sensor.StateText->SetColorAndOpacity(FSlateColor(FLinearColor::White));
		}
	}
}
