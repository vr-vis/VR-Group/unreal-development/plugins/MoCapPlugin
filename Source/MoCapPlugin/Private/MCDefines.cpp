#include "MCDefines.h"

FSensorDataEntry* FSensorData::GetEntry(EBodyPart BodyPart) {
	switch (BodyPart) {
	case EBodyPart::Head:
		return &Head;
	case EBodyPart::HandL:
		return &HandL;
	case EBodyPart::HandR:
		return &HandR;
	case EBodyPart::LowerArmL:
		return &LowerArmL;
	case EBodyPart::LowerArmR:
		return &LowerArmR;
	case EBodyPart::LowerLegL:
		return &LowerLegL;
	case EBodyPart::LowerLegR:
		return &LowerLegR;
	case EBodyPart::UpperBody:
		return &UpperBody;
	case EBodyPart::LowerBody:
		return &LowerBody;
	default:
		return nullptr;
	}
}
