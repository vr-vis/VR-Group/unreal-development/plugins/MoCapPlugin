// Fill out your copyright notice in the Description page of Project Settings.


#include "MCScaleNode.h"

#include "Engine/GameEngine.h"

void FMCScaleNode::InitializeBoneReferences(const FBoneContainer& RequiredBones) {
    DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(InitializeBoneReferences)

    if (BoneToModify.BoneIndex == -1) {
        BoneToModify = FBoneReference("root");
    }

    BoneToModify.Initialize(RequiredBones);
}

FMCScaleNode::FMCScaleNode() {
    pawn = nullptr;
}

void FMCScaleNode::EvaluateSkeletalControl_AnyThread(FComponentSpacePoseContext& Output, TArray<FBoneTransform>& OutBoneTransforms) {
    DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(EvaluateSkeletalControl_AnyThread)
    check(OutBoneTransforms.Num() == 0);

    if (pawn == nullptr || Alpha == 0.f) {
        return;
    }

    const FBoneContainer& BoneContainer = Output.Pose.GetPose().GetBoneContainer();

    const BoneNames& names = pawn->BoneNames;
    USkeletalMeshComponent* skeletalMesh = pawn->SkeletalMesh;
    float orHipNeck = skeletalMesh->GetSocketLocation(names.neck).Z - skeletalMesh->GetSocketLocation(names.pelvis).Z;
    float appliedScale = measurements.NeckToHip / orHipNeck;

    FCompactPoseBoneIndex BoneIndex = BoneToModify.GetCompactPoseIndex(BoneContainer);
    FTransform BoneTrans = Output.Pose.GetComponentSpaceTransform(BoneIndex);
    BoneTrans.SetScale3D(FVector(appliedScale, appliedScale, appliedScale));
    OutBoneTransforms.Add(FBoneTransform(BoneIndex, BoneTrans));

}

bool FMCScaleNode::IsValidToEvaluate(const USkeleton* Skeleton, const FBoneContainer& RequiredBones) {
    bool valid = BoneToModify.IsValidToEvaluate(RequiredBones);
    return valid;
}

void FMCScaleNode::GatherDebugData(FNodeDebugData& DebugData) {
    FString DebugLine = DebugData.GetNodeName(this);

    DebugData.AddDebugItem(DebugLine);

    ComponentPose.GatherDebugData(DebugData);
}