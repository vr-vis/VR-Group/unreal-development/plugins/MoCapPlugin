// Fill out your copyright notice in the Description page of Project Settings.


#include "MCFeedbackWidget.h"

UMCFeedbackWidget::UMCFeedbackWidget(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{}

void UMCFeedbackWidget::NativeConstruct() {
	Super::NativeConstruct();
}

void UMCFeedbackWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime) {
	Super::NativeTick(MyGeometry, InDeltaTime);
}

void UMCFeedbackWidget::InitSensorBox(FSensorSetup& SensorSetup) {
	
	if (!SensorBox) {
		return;
	}

	SensorSetup.Head.StateText = WidgetTree->ConstructWidget<UTextBlock>(UTextBlock::StaticClass());
	SensorBox->AddChild(SensorSetup.Head.StateText);
	
	SensorSetup.HandL.StateText = WidgetTree->ConstructWidget<UTextBlock>(UTextBlock::StaticClass());
	SensorBox->AddChild(SensorSetup.HandL.StateText);
	
	SensorSetup.HandR.StateText = WidgetTree->ConstructWidget<UTextBlock>(UTextBlock::StaticClass());
	SensorBox->AddChild(SensorSetup.HandR.StateText);
	
	for (FSensor& Sensor : SensorSetup.Trackers) {
		Sensor.StateText = WidgetTree->ConstructWidget<UTextBlock>(UTextBlock::StaticClass());
		SensorBox->AddChild(Sensor.StateText);
	}

	inited = true;

}
