// Fill out your copyright notice in the Description page of Project Settings.


#include "MCLogHandler.h"

#include <string>

#include "IUniversalLogging.h"
#include "Engine/Engine.h"

#include "JsonUtilities/Public/JsonUtilities.h"


FString UMCLogHandler::GetSessionIdentifier() {
	return UniLog.GetSessionIdentifier();
}

void UMCLogHandler::NewLog(const FString& Name) {
	UniLog.NewLogStream(Name, "Saved/OwnLogs/", Name + ".log", true);
	CurLog = Name;
}

void UMCLogHandler::LogData(const FString& Message) {
	FDateTime Now = FDateTime::Now();
	FString TimeStamp = FString::FromInt(Now.GetMinute()) + ":" +
		FString::FromInt(Now.GetSecond()) + ":" +
		FString::FromInt(Now.GetMillisecond()) + " ";

    DataLogArray.Add(TimeStamp + Message);
}

void UMCLogHandler::WriteToLogFile() {
    for (auto EntryMessage : DataLogArray) {
	    UniLog.Log(EntryMessage, CurLog);
    }
	DataLogArray.Empty();
}

struct DirectoryVisitor : public IPlatformFile::FDirectoryVisitor {
	TArray<FString> Files;

	bool Visit(const TCHAR* FilenameOrDirectory, bool bIsDirectory) override {
		if (bIsDirectory) {
			Files.Add(FilenameOrDirectory);
		}
		return true;
	}
};

#define LOCTEXT_NAMESPACE "AlertWidget"

void UMCLogHandler::CopyLogToRecordings(FString& Name) {

	if (!FPaths::DirectoryExists(FPaths::ProjectSavedDir() + "/Recordings")) {
		IPlatformFile& PlatformFile = FPlatformFileManager::Get().GetPlatformFile();
		PlatformFile.CreateDirectory(*(FPaths::ProjectSavedDir() + "/Recordings"));
	}

	int LastSubIndex = Name.Find("_", ESearchCase::CaseSensitive, ESearchDir::FromEnd);
	if (Name.RightChop(LastSubIndex + 1).IsNumeric()) {
		Name = Name.Left(LastSubIndex);
	}

	FString FolderName;
	int j = -1;
	do {
		if (j == -1) {
			FolderName = Name;
		}
		else if (j < 9) {
			FolderName = Name + "_0" + FString::FromInt(j + 1);
		}
		else {
			FolderName = Name + "_" + FString::FromInt(j + 1);
		}
		j++;
	} while (FPaths::DirectoryExists(FPaths::ProjectSavedDir() + "/Recordings/" + FolderName));

	Name = FolderName;	

	const FString Path = FPaths::ProjectSavedDir() + "Recordings/" + Name + "/";
	const FString PathLog = FPaths::ConvertRelativePathToFull(FPaths::ProjectDir() + "Saved/OwnLogs");

	DirectoryVisitor Visitor;
	IFileManager::Get().IterateDirectory(*PathLog, Visitor);
	FFileManagerGeneric Fm;
	FDateTime MaxNonStoppedDateTime;
	FDateTime MaxDateTime;
	int MaxNonStoppedIndex = -1;
	int MaxIndex = -1;
	for (int i = 0; i < Visitor.Files.Num(); i++) {
		FDateTime DateTime = Fm.GetTimeStamp(*Visitor.Files[i]);
		if ((DateTime > MaxNonStoppedDateTime || MaxNonStoppedIndex == -1) && !Visitor.Files[i].Contains("Stopped")) {
			MaxNonStoppedDateTime = DateTime;
			MaxNonStoppedIndex = i;
		}
		if (DateTime > MaxDateTime || MaxIndex == -1) {
			MaxDateTime = DateTime;
			MaxIndex = i;
		}
	}

	FString PathMostRecent = Visitor.Files[MaxNonStoppedIndex];
	LastSubIndex = PathMostRecent.Find("/", ESearchCase::CaseSensitive, ESearchDir::FromEnd);
	PathMostRecent = PathMostRecent.RightChop(LastSubIndex + 1);
	PathMostRecent = PathMostRecent + "/" + CurLog + ".log";
	if (MaxIndex != MaxNonStoppedIndex) {
		FString PathStopped = Visitor.Files[MaxIndex];
		LastSubIndex = PathStopped.Find("/", ESearchCase::CaseSensitive, ESearchDir::FromEnd);
		PathStopped = PathStopped.RightChop(LastSubIndex + 1);
		PathStopped = PathStopped + "/" + CurLog + ".log";
		PathMostRecent += "\n" + PathStopped;
	}
	FFileHelper::SaveStringToFile(PathMostRecent, *(Path + "LogSourcePath.txt"));

}

#undef LOCTEXT_NAMESPACE

void UMCLogHandler::StartRecording() {
	TSharedPtr<FJsonObject> Json = MAKE_JSON;
	Json->SetStringField("Type", "Start");
	LogData(MCUtils::JsonToString(Json));
}

void UMCLogHandler::StopRecording() {
	TSharedPtr<FJsonObject> Json = MAKE_JSON;
	Json->SetStringField("Type", "End");
	LogData(MCUtils::JsonToString(Json));
}

void UMCLogHandler::SetMarker() {
	TSharedPtr<FJsonObject> Json = MAKE_JSON;
	Json->SetStringField("Type", "Marker");
	LogData(MCUtils::JsonToString(Json));
}


