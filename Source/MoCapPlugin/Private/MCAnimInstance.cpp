// Fill out your copyright notice in the Description page of Project Settings.


#include "MCAnimInstance.h"
#include "MCPawn.h"

void UMCAnimInstance::NativeInitializeAnimation() {
	Super::NativeInitializeAnimation();

	MaxWaitTicks = 0;
	WaitTicks = 0;
	Waiting = false;

	AppliedPose = true;
	DoRig = false;

	Scale = FVector(1., 1., 1.);

}

void UMCAnimInstance::NativeUpdateAnimation(float DeltaSeconds) {
	Super::NativeUpdateAnimation(DeltaSeconds);
	
	if (!AppliedPose) {
		if (Waiting) {
			if (WaitTicks >= MaxWaitTicks) {
				FPoseSnapshot Snapshot;
				SnapshotPose(Snapshot);
				SnapshotAnimations.Last().Snapshots.Add(Snapshot);
				Waiting = false;
				AppliedPose = true;
				WaitTicks = 0;
			}
			else {
				WaitTicks ++;
			}
		}
		else {
			Waiting = true;
			WaitTicks = 0;
		}
	}


}

const FSensorDataEntry& UMCAnimInstance::GetSensorData(TEnumAsByte<EBodyPart> BodyPart) {
	switch (BodyPart) {
	case EBodyPart::Head:
		return SensorData.Head;
	case EBodyPart::HandL:
		return SensorData.HandL;
	case EBodyPart::HandR:
		return SensorData.HandR;
	case EBodyPart::LowerArmL:
		return SensorData.LowerArmL;
	case EBodyPart::LowerArmR:
		return SensorData.LowerArmR;
	case EBodyPart::LowerLegL:
		return SensorData.LowerLegL;
	case EBodyPart::LowerLegR:
		return SensorData.LowerLegR;
	case EBodyPart::UpperBody:
		return SensorData.UpperBody;
	case EBodyPart::LowerBody:
		return SensorData.LowerBody;

	default:
		return SensorData.Head;

	}
}

void UMCAnimInstance::SetSensorData(EBodyPart BodyPart, const FVector& Pos, const FQuat& Rot) {

	FSensorDataEntry* entry = nullptr;

	switch (BodyPart) {
	case EBodyPart::Head:
		entry = &SensorData.Head;
		break;
	case EBodyPart::HandL:
		entry = &SensorData.HandL;
		break;
	case EBodyPart::HandR:
		entry = &SensorData.HandR;
		break;
	case EBodyPart::LowerArmL:
		entry = &SensorData.LowerArmL;
		break;
	case EBodyPart::LowerArmR:
		entry = &SensorData.LowerArmR;
		break;
	case EBodyPart::LowerLegL:
		entry = &SensorData.LowerLegL;
		break;
	case EBodyPart::LowerLegR:
		entry = &SensorData.LowerLegR;
		break;
	case EBodyPart::UpperBody:
		entry = &SensorData.UpperBody;
		break;
	case EBodyPart::LowerBody:
		entry = &SensorData.LowerBody;
		break;

	}

	entry->Pos = Pos;
	entry->Rot = Rot;
	entry->PosUsed = true;
	entry->valid = true;

}

void UMCAnimInstance::SetSensorData(EBodyPart BodyPart, const FQuat& Rot) {
	
	FSensorDataEntry* entry = nullptr;

	switch (BodyPart) {
	case EBodyPart::Head:
		entry = &SensorData.Head;
		break;
	case EBodyPart::HandL:
		entry = &SensorData.HandL;
		break;
	case EBodyPart::HandR:
		entry = &SensorData.HandR;
		break;
	case EBodyPart::LowerArmL:
		entry = &SensorData.LowerArmL;
		break;
	case EBodyPart::LowerArmR:
		entry = &SensorData.LowerArmR;
		break;
	case EBodyPart::LowerLegL:
		entry = &SensorData.LowerLegL;
		break;
	case EBodyPart::LowerLegR:
		entry = &SensorData.LowerLegR;
		break;
	case EBodyPart::UpperBody:
		entry = &SensorData.UpperBody;
		break;
	case EBodyPart::LowerBody:
		entry = &SensorData.LowerBody;
		break;

	}

	entry->Rot = Rot;
	entry->PosUsed = false;
	entry->valid = true;
}

void UMCAnimInstance::SetSensorOffset(EBodyPart BodyPart, const FTransform& DeltaTransform) {

	FSensorOffset* entry = nullptr;

	switch (BodyPart) {
	case EBodyPart::Head:
		entry = &SensorOffsets.Head;
		break;
	case EBodyPart::HandL:
		entry = &SensorOffsets.HandL;
		break;
	case EBodyPart::HandR:
		entry = &SensorOffsets.HandR;
		break;
	case EBodyPart::LowerLegL:
		entry = &SensorOffsets.LowerLegL;
		break;
	case EBodyPart::LowerLegR:
		entry = &SensorOffsets.LowerLegR;
		break;
	case EBodyPart::LowerArmL:
		entry = &SensorOffsets.LowerArmL;
		break;
	case EBodyPart::LowerArmR:
		entry = &SensorOffsets.LowerArmR;
		break;
	case EBodyPart::UpperBody:
		entry = &SensorOffsets.UpperBody;
		break;
	case EBodyPart::LowerBody:
		entry = &SensorOffsets.LowerBody;
		break;

	}

	entry->DeltaTransform = DeltaTransform;

}
