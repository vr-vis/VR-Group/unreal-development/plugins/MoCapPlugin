// Fill out your copyright notice in the Description page of Project Settings.


#include "MCCalibrationNode.h"

#include "Engine/GameEngine.h"

bool FMCCalibrationNode::SetUpSkeletonBones() {

    if (pawn != nullptr
        && (BonesToModify.Num() == 0)) {

        const BoneNames& names = pawn->BoneNames;

        BonesToModify.Empty();

        BonesToModify.Add(FBoneReference(names.boneroot));
        BonesToModify.Add(FBoneReference(names.spine01));
        BonesToModify.Add(FBoneReference(names.spine02));
        BonesToModify.Add(FBoneReference(names.spine03));
        BonesToModify.Add(FBoneReference(names.spine04));
        BonesToModify.Add(FBoneReference(names.spine05));

        BonesToModify.Add(FBoneReference(names.neck));
        BonesToModify.Add(FBoneReference(names.neck02));
        BonesToModify.Add(FBoneReference(names.head));

        BonesToModify.Add(FBoneReference(names.clavicle_l));
        BonesToModify.Add(FBoneReference(names.clavicle_r));
        BonesToModify.Add(FBoneReference(names.upperarm_l));
        BonesToModify.Add(FBoneReference(names.upperarm_r));
        BonesToModify.Add(FBoneReference(names.lowerarm_l));
        BonesToModify.Add(FBoneReference(names.lowerarm_r));
        BonesToModify.Add(FBoneReference(names.hand_l));
        BonesToModify.Add(FBoneReference(names.hand_r));
       
        BonesToModify.Add(FBoneReference(names.pelvis));
        BonesToModify.Add(FBoneReference(names.calf_l));
        BonesToModify.Add(FBoneReference(names.calf_r));
        BonesToModify.Add(FBoneReference(names.thigh_l));
        BonesToModify.Add(FBoneReference(names.thigh_r));
        BonesToModify.Add(FBoneReference(names.foot_l));
        BonesToModify.Add(FBoneReference(names.foot_r));
        BonesToModify.Add(FBoneReference(names.foot_socket_l));
        BonesToModify.Add(FBoneReference(names.foot_socket_r));

        return true;

    }

    return false;
}

struct FCompareBoneTransformIndexMeasurements {
    FORCEINLINE bool operator()(const FMCAppliedLength& A, const FMCAppliedLength& B) const
    {
        return A.index < B.index;
    }
};

void FMCCalibrationNode::CalcAppliedLengths(const FBoneContainer& RequiredBones, FBodyProportionStruct& measurements) {

    if (pawn == nullptr) {
        return;
    }

    const BoneNames& names = pawn->BoneNames;
    USkeletalMeshComponent* skeletalMesh = pawn->SkeletalMesh;

    measurements.appliedLengths.Empty();


    //ARMS

    AddAppliedLength(names.lowerarm_l, names.hand_l, measurements.LowerArm, measurements, skeletalMesh, RequiredBones);
    AddAppliedLength(names.lowerarm_r, names.hand_r, measurements.LowerArm, measurements, skeletalMesh, RequiredBones);

    AddAppliedLength(names.upperarm_l, names.lowerarm_l, measurements.UpperArm, measurements, skeletalMesh, RequiredBones);
    AddAppliedLength(names.upperarm_r, names.lowerarm_r, measurements.UpperArm, measurements, skeletalMesh, RequiredBones);

    float orShoulderDist = FVector::Dist(skeletalMesh->GetSocketLocation(names.upperarm_l), skeletalMesh->GetSocketLocation(names.upperarm_r));
    float orClavicleShoulderDist = FVector::Dist(skeletalMesh->GetSocketLocation(names.upperarm_l), skeletalMesh->GetSocketLocation(names.clavicle_l));
    float alphaShoulder = measurements.ShoulderToShoulder / orShoulderDist;
    AddAppliedLength(names.clavicle_l, names.upperarm_l, alphaShoulder * orClavicleShoulderDist, measurements, skeletalMesh, RequiredBones);
    AddAppliedLength(names.clavicle_r, names.upperarm_r, alphaShoulder * orClavicleShoulderDist, measurements, skeletalMesh, RequiredBones);

    //LEGS

    AddAppliedLength(names.calf_l, names.foot_l, measurements.LowerLeg, measurements, skeletalMesh, RequiredBones);
    AddAppliedLength(names.calf_r, names.foot_r, measurements.LowerLeg, measurements, skeletalMesh, RequiredBones);

    AddAppliedLength(names.thigh_l, names.calf_l, measurements.UpperLeg, measurements, skeletalMesh, RequiredBones);
    AddAppliedLength(names.thigh_r, names.calf_r, measurements.UpperLeg, measurements, skeletalMesh, RequiredBones);

    AddAppliedLength(names.pelvis, names.thigh_l, measurements.HipWidth / 2.f, measurements, skeletalMesh, RequiredBones);
    AddAppliedLength(names.pelvis, names.thigh_r, measurements.HipWidth / 2.f, measurements, skeletalMesh, RequiredBones);


    //HEIGHT

    //AddAppliedLength(names.boneroot, names.pelvis, measurements.HipHeight, measurements, skeletalMesh, RequiredBones);

    //AddAppliedLength(names.neck, names.head, measurements.HeadToNeck, measurements, skeletalMesh, RequiredBones);

    AddAppliedLength(names.pelvis, names.spine01, GetOriginalDistance(skeletalMesh, names.pelvis, names.spine01), measurements, skeletalMesh, RequiredBones);
    AddAppliedLength(names.spine01, names.spine02, GetOriginalDistance(skeletalMesh, names.spine01, names.spine02), measurements, skeletalMesh, RequiredBones);
    AddAppliedLength(names.spine02, names.spine03, GetOriginalDistance(skeletalMesh, names.spine02, names.spine03), measurements, skeletalMesh, RequiredBones);
    AddAppliedLength(names.spine03, names.neck, GetOriginalDistance(skeletalMesh, names.spine03, names.neck), measurements, skeletalMesh, RequiredBones);

    measurements.appliedLengths.Sort(FCompareBoneTransformIndexMeasurements());
    if (measurements.loaded) {
        measurements.calcedAppliedLengths = true;
    }
}

void FMCCalibrationNode::AddAppliedLength(const FName& startBone, const FName& endBone, float appliedLength, FBodyProportionStruct& measurements, const USkeletalMeshComponent* skeletalMesh, const FBoneContainer& RequiredBones) {
    int boneIndex = -1;
    for (int j = 0; j < BonesToModify.Num(); j++) {
        if (endBone.Compare(BonesToModify[j].BoneName) == 0) {
            boneIndex = j;
            break;
        }
    }
    if (boneIndex == -1) return;
    FCompactPoseBoneIndex boneToModify = BonesToModify[boneIndex].GetCompactPoseIndex(RequiredBones);

    FMCAppliedLength l;
    l.applied = std::abs(appliedLength);
    l.original = GetOriginalDistance(skeletalMesh, startBone, endBone);
    l.endBone = endBone;
    l.index = boneToModify;
    measurements.appliedLengths.Emplace(l);
}

float FMCCalibrationNode::GetOriginalDistance(const USkeletalMeshComponent* skeletalMesh, const FName& startBone, const FName& endBone) {
    return UKismetAnimationLibrary::K2_DistanceBetweenTwoSocketsAndMapRange(
        skeletalMesh,
        startBone, ERelativeTransformSpace::RTS_World,
        endBone, ERelativeTransformSpace::RTS_World,
        false, 0, 0, 0, 0);
}

void FMCCalibrationNode::InitializeBoneReferences(const FBoneContainer& RequiredBones) {
    DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(InitializeBoneReferences)
    for (int i = 0; i < BonesToModify.Num(); i++) {
        BonesToModify[i].Initialize(RequiredBones);
        if (!BonesToModify[i].IsValidToEvaluate(RequiredBones)) {
            BonesToModify.RemoveAt(i);
            i--;
        }
    }
}

FMCCalibrationNode::FMCCalibrationNode() {
    pawn = nullptr;
}

void FMCCalibrationNode::EvaluateSkeletalControl_AnyThread(FComponentSpacePoseContext& Output, TArray<FBoneTransform>& OutBoneTransforms) {
    DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(EvaluateSkeletalControl_AnyThread)
    check(OutBoneTransforms.Num() == 0);

    if (pawn == nullptr || Alpha == 0.f) {
        return;
    }

    const FBoneContainer& BoneContainer = Output.Pose.GetPose().GetBoneContainer();
    TMap<int32, FVector> transformed;

    bool bonesChanged = SetUpSkeletonBones();
    if (bonesChanged) {
        InitializeBoneReferences(BoneContainer);
    }

    FBodyProportionStruct& measurements = pawn->GetAnimInstance()->Measurements;

    if (!measurements.calcedAppliedLengths) {
        if (reverse) {
            return;
        }
        CalcAppliedLengths(BoneContainer, measurements);
    }

    for (int i = 0; i < measurements.appliedLengths.Num(); i++) {

        FMCAppliedLength& measurement = measurements.appliedLengths[i];

        //---get name and index of current bone---
        FName bName = measurement.endBone;

        FCompactPoseBoneIndex boneToModify = measurement.index;

        if (boneToModify.IsRootBone()) {
            continue;
        }

        //---set bone length---
        FTransform BoneTrans = Output.Pose.GetComponentSpaceTransform(boneToModify);
        FVector BonePos = BoneTrans.GetLocation();
        FVector ParentBonePos = FVector::ZeroVector;
        if (!boneToModify.IsRootBone()) {
            ParentBonePos = Output.Pose.GetComponentSpaceTransform(Output.Pose.GetPose().GetParentBoneIndex(boneToModify)).GetLocation();
        }
        else {
            ParentBonePos = BonePos - FVector(0, 0, BonePos.Z);
            if (BonePos.Z == 0) {
                ParentBonePos.Z -= 0.1;
            }
        }
        FVector ParentToCurrentUV = (BonePos - ParentBonePos).GetUnsafeNormal();

        FVector* res = nullptr;
        if (!boneToModify.IsRootBone()) {
            res = transformed.Find(Output.Pose.GetPose().GetParentBoneIndex(boneToModify).GetInt());
        }

        float TargetLength = measurement.applied;
        if (reverse) {
            TargetLength = measurement.original;
        }

        FVector newLoc = ParentBonePos + ParentToCurrentUV * TargetLength;
        if (res != nullptr) {
            newLoc = *res + ParentToCurrentUV * TargetLength;
        }

        transformed.Add(boneToModify.GetInt(), newLoc);

        BoneTrans.SetLocation(newLoc);

        OutBoneTransforms.Add(FBoneTransform(boneToModify, BoneTrans));
    }


    OutBoneTransforms.Sort(FCompareBoneTransformIndex());
}

bool FMCCalibrationNode::IsValidToEvaluate(const USkeleton* Skeleton, const FBoneContainer& RequiredBones) {
    for (auto& bone : BonesToModify) {
        if (!bone.IsValidToEvaluate(RequiredBones)) return false;
    }
    return true;
}

void FMCCalibrationNode::GatherDebugData(FNodeDebugData& DebugData) {
    FString DebugLine = DebugData.GetNodeName(this);

    DebugData.AddDebugItem(DebugLine);

    ComponentPose.GatherDebugData(DebugData);
}