// Fill out your copyright notice in the Description page of Project Settings.


#include "MCInstructionWidget.h"

UMCInstructionWidget::UMCInstructionWidget(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{}

void UMCInstructionWidget::NativeConstruct() {
	Super::NativeConstruct();
}

void UMCInstructionWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime) {
	Super::NativeTick(MyGeometry, InDeltaTime);
}