// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "MoCapPlugin.h"

#include "IDisplayCluster.h"						   // IsMaster?
#include "Cluster/IDisplayClusterClusterManager.h"	   // IsMaster?

#define LOCTEXT_NAMESPACE "FMoCapPluginModule"

bool FMoCapPluginModule::bPluginInitialized = false;
bool FMoCapPluginModule::bIsMaster = false;

void FMoCapPluginModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
}

void FMoCapPluginModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
}

void FMoCapPluginModule::AskForSettings()
{
	bIsMaster = IDisplayCluster::Get().GetClusterMgr() != nullptr && IDisplayCluster::Get().GetClusterMgr()->IsMaster();
	bPluginInitialized = true;
}

bool FMoCapPluginModule::GetIsMaster()
{
	if (!bPluginInitialized)
	{
		AskForSettings();
	}

	return bIsMaster;
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FMoCapPluginModule, MoCapPlugin)