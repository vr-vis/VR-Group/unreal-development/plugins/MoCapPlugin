// Fill out your copyright notice in the Description page of Project Settings.


#include "Modificators/SmoothFingerData.h"

ASmoothFingerData::ASmoothFingerData()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

void ASmoothFingerData::BeginPlay()
{
	Super::BeginPlay();
}

void ASmoothFingerData::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

float ASmoothFingerData::smoothFloat(TArray<float> values)
{
	float ret = 0;
	for (int i = 0; i < values.Num(); i++) {
		ret += values[i];
	}
	return ret / values.Num();
}

TArray<float> ASmoothFingerData::getFingerList(EHandPart hp, bool rightHand, int step)
{
	TArray<float> ret;
	switch (hp) {
	case Thumb:
		ret.Add(getHandPartData(EHandPart::Thumb, rightHand, &ASS->AnimData[step].FingerData));
		//ret.Add(getHandPartData(EHandPart::Index, rightHand, &ASS->AnimData[step].FingerData));
		break;
	case Index:
		ret.Add(getHandPartData(EHandPart::Thumb, rightHand, &ASS->AnimData[step].FingerData));
		ret.Add(getHandPartData(EHandPart::Index, rightHand, &ASS->AnimData[step].FingerData));
		ret.Add(getHandPartData(EHandPart::Middle, rightHand, &ASS->AnimData[step].FingerData));
		break;
	case Middle:
		ret.Add(getHandPartData(EHandPart::Index, rightHand, &ASS->AnimData[step].FingerData));
		ret.Add(getHandPartData(EHandPart::Middle, rightHand, &ASS->AnimData[step].FingerData));
		ret.Add(getHandPartData(EHandPart::Ring, rightHand, &ASS->AnimData[step].FingerData));
		break;
	case Ring: 
		ret.Add(getHandPartData(EHandPart::Middle, rightHand, &ASS->AnimData[step].FingerData));
		ret.Add(getHandPartData(EHandPart::Ring, rightHand, &ASS->AnimData[step].FingerData));
		ret.Add(getHandPartData(EHandPart::Pinky, rightHand, &ASS->AnimData[step].FingerData));
		break;
	case Pinky:
		ret.Add(getHandPartData(EHandPart::Ring, rightHand, &ASS->AnimData[step].FingerData));
		ret.Add(getHandPartData(EHandPart::Pinky, rightHand, &ASS->AnimData[step].FingerData));
		break;

	case Thumb_Index:
		ret.Add(getHandPartData(EHandPart::Thumb_Index, rightHand, &ASS->AnimData[step].FingerData));
		//ret.Add(getHandPartData(EHandPart::Index_Middle, rightHand, &ASS->AnimData[step].FingerData));
		break;
	case Index_Middle:
		ret.Add(getHandPartData(EHandPart::Thumb_Index, rightHand, &ASS->AnimData[step].FingerData));
		ret.Add(getHandPartData(EHandPart::Index_Middle, rightHand, &ASS->AnimData[step].FingerData));
		ret.Add(getHandPartData(EHandPart::Middle_Ring, rightHand, &ASS->AnimData[step].FingerData));
		break;
	case Middle_Ring:
		ret.Add(getHandPartData(EHandPart::Index_Middle, rightHand, &ASS->AnimData[step].FingerData));
		ret.Add(getHandPartData(EHandPart::Middle_Ring, rightHand, &ASS->AnimData[step].FingerData));
		ret.Add(getHandPartData(EHandPart::Ring_Pinky, rightHand, &ASS->AnimData[step].FingerData));
		break;
	case Ring_Pinky:
		ret.Add(getHandPartData(EHandPart::Middle_Ring, rightHand, &ASS->AnimData[step].FingerData));
		ret.Add(getHandPartData(EHandPart::Ring_Pinky, rightHand, &ASS->AnimData[step].FingerData));
		break;
	default:
		break;
	}
	return ret;
}

void ASmoothFingerData::smoothHand(bool rightHand, int step)
{
	TArray<float> newValues;
	for (int j = 0; j < EHandPart::Hand_LAST; j++) {
		newValues.Add(smoothFloat(getFingerList(EHandPart(j), rightHand, step)));
	}
	for (int j = 0; j < EHandPart::Hand_LAST; j++) {
		setHandPartData(EHandPart(j), rightHand, &ASS->AnimData[step].FingerData, newValues[j]);
	}

}

void ASmoothFingerData::ApplyModification()
{
	smooth();
}

void ASmoothFingerData::smooth()
{
	if (!ASS) {
		return;
		//ToDo Print Error Message
	}
	for (int i = 0; i < ASS->AnimData.Num(); i++) {
		smoothHand(true, i);
		smoothHand(false, i);
	}
}
