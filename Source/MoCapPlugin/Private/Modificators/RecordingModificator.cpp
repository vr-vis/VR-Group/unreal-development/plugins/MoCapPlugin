// Fill out your copyright notice in the Description page of Project Settings.


#include "Modificators/RecordingModificator.h"

// Sets default values
ARecordingModificator::ARecordingModificator()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	// PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ARecordingModificator::BeginPlay()
{
	Super::BeginPlay();
	
	APlayerController* MyPlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	EnableInput(MyPlayerController);

}

// Called every frame
void ARecordingModificator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ARecordingModificator::SetAssAndExecuteModification(FAnimSaveState* ass, TArray<FManipulation>* trackedModifications)
{
	SetAnimSaveState(ass);
	SetTrackedModifications(trackedModifications);
	Modify();
}

void ARecordingModificator::SetAnimSaveState(FAnimSaveState* ass)
{
	ASS = ass;
}

void ARecordingModificator::SetTrackedModifications(TArray<FManipulation>* trackedModifications)
{
	TrackedModifications = trackedModifications;
}

void ARecordingModificator::ApplyModification()
{
	//ToDo ExecuteModification
}

void ARecordingModificator::setTrackingVar(TArray<FManipulation>* trackedModifications)
{
	TrackedModifications = trackedModifications;
}

FAnimSaveState* ARecordingModificator::GetAnimSaveState()
{
	return ASS;
}

void ARecordingModificator::ExecuteMaipulation(FManipulation* manipulation)
{
	//ToDo: Check somewhere if nessesarry variables were set
	if (manipulation->Typ == TimeShift) {
		ShiftTime(manipulation->duration, manipulation->startStep, manipulation->endStep);
	}
	if (manipulation->Typ == RotationShift) {
		ShiftRot(manipulation->bodyPartToShift, manipulation->rotQuat, manipulation->startStep, manipulation->endStep);
	}
	if (manipulation->Typ == PositionShift) {
		ShiftPos(manipulation->bodyPartToShift, manipulation->posVec, manipulation->startStep, manipulation->endStep);
	}
	if (manipulation->Typ == DuplicateStep) {
		duplicateStep(manipulation->startStep);
	}
	if (manipulation->Typ == FingerShift) {
		ShiftFinger(manipulation->handPartToShift, manipulation->fingerMovement, manipulation->rightHand, manipulation->startStep, manipulation->endStep);
	}
	if (manipulation->Typ == SetPosition) {
		SetPositions(manipulation->bodyPartToShift, manipulation->posVec, manipulation->startStep, manipulation->endStep);
	}
	if (manipulation->Typ == SetRotation) {
		SetRotations(manipulation->bodyPartToShift, manipulation->rotQuat, manipulation->startStep, manipulation->endStep);
	}
}

void ARecordingModificator::ExecuteMaipulations(TArray<FManipulation>* manipulationList)
{
	for (int i = 0; i < manipulationList->Num(); i++) {
		
		ExecuteMaipulation(&(*manipulationList)[i]);

	}
}

void ARecordingModificator::ShiftTime(FTimespan t, int firstStep, int lastStep)
{
	firstStep = prepareStart(firstStep);
	lastStep = prepareEnd(lastStep);

	for (int currentStep = firstStep; currentStep <= lastStep; currentStep++) {
		SetTime(t+GetTime(currentStep), currentStep);
	}

	if (t > 0) {
		addSteps(firstStep, t);
	}
	deleteTimeOverlappingSteps();
}

void ARecordingModificator::ShiftRot(EBodyPart bodyPart, FQuat additionalRotQuat, int firstStep, int lastStep)
{
	firstStep = prepareStart(firstStep);
	lastStep = prepareEnd(lastStep);

	for (int currentStep = firstStep; currentStep <= lastStep; currentStep++) {
		FQuat newQuat = getBodyPartSensorData(bodyPart, &ASS->AnimData[currentStep].SensorData)->Rot + additionalRotQuat;
		SetRot(bodyPart, newQuat, currentStep);
	}
}

void ARecordingModificator::SetRotations(EBodyPart bodyPart, FQuat rotQuat, int firstStep, int lastStep)
{
	firstStep = prepareStart(firstStep);
	lastStep = prepareEnd(lastStep);

	for (int currentStep = firstStep; currentStep <= lastStep; currentStep++) {
		SetRot(bodyPart, rotQuat, currentStep);
	}
}

void ARecordingModificator::ShiftPos(EBodyPart bodyPart, FVector additionalPosVec, int firstStep, int lastStep)
{
	firstStep = prepareStart(firstStep);
	lastStep = prepareEnd(lastStep);

	for (int currentStep = firstStep; currentStep <= lastStep; currentStep++) {
		FVector newPos = getBodyPartSensorData(bodyPart, &ASS->AnimData[currentStep].SensorData)->Pos + additionalPosVec;
		SetPos(bodyPart, newPos, currentStep);
	}
}

void ARecordingModificator::SetPositions(EBodyPart bodyPart, FVector posVec, int firstStep, int lastStep)
{
	firstStep = prepareStart(firstStep);
	lastStep = prepareEnd(lastStep);

	for (int currentStep = firstStep; currentStep <= lastStep; currentStep++) {
		SetPos(bodyPart, posVec, currentStep);
	}
}

void ARecordingModificator::ShiftFinger(EHandPart handPart, float additionalMovement, bool rightHand, int firstStep, int lastStep)
{
	firstStep = prepareStart(firstStep);
	lastStep = prepareEnd(lastStep);
	for (int i = firstStep; i <= lastStep; i++) {
		float oldValue = getHandPartData(handPart, rightHand, &ASS->AnimData[i].FingerData);
		SetFingerData(handPart, rightHand, oldValue + additionalMovement, i);
	}
	
}

void ARecordingModificator::addStep(FProcessedAnimData step, int position)
{
	ASS->AnimData.Insert(step, position);
}

void ARecordingModificator::deleteStep(int position)
{
	ASS->AnimData.RemoveAt(position);
}

void ARecordingModificator::duplicateStep(int position)
{
	FProcessedAnimData step = ASS->AnimData[position];
	addStep(step, position);

	if (TrackedModifications) {
		FManipulation manip;
		manip.Typ = FModType::DuplicateStep;
		manip.startStep = position;
		TrackedModifications->Add(manip);
	}
}

int ARecordingModificator::prepareStart(int start)
{
	if (start < 0) {
		start = 0;
	}
	if (start >= ASS->AnimData.Num()) {
		start = ASS->AnimData.Num() - 1;
	}
	return start;
}

int ARecordingModificator::prepareEnd(int end)
{
	if (end < 0) {
		end = ASS->AnimData.Num() - 1;
	}
	if (end >= ASS->AnimData.Num()) {
		end = ASS->AnimData.Num() - 1;
	}
	return end;
}

FSensorDataEntry* ARecordingModificator::getBodyPartSensorData(EBodyPart BodyPart, FSensorData* sensData)
{
	switch (BodyPart) {
	case EBodyPart::Head:
		return &sensData->Head;
	case EBodyPart::HandL:
		return &sensData->HandL;
	case EBodyPart::HandR:
		return &sensData->HandR;
	case EBodyPart::LowerArmL:
		return &sensData->LowerArmL;
	case EBodyPart::LowerArmR:
		return &sensData->LowerArmR;
	case EBodyPart::LowerLegL:
		return &sensData->LowerLegL;
	case EBodyPart::LowerLegR:
		return &sensData->LowerLegR;
	case EBodyPart::UpperBody:
		return &sensData->UpperBody;
	case EBodyPart::LowerBody:
		return &sensData->LowerBody;
	default:
		return nullptr;
	}
}

FTimespan ARecordingModificator::getTimePerStep()
{
	return(ASS->AnimData[ASS->AnimData.Num()-1].Timestamp - ASS->AnimData[0].Timestamp)/ ASS->AnimData.Num();
}

void ARecordingModificator::addSteps(int position, FTimespan timeLength)
{
	FTimespan timePerStep = getTimePerStep();
	int addAtStep = position;
	for (FTimespan i = 0; i < timeLength; i += timePerStep) {
		//ToDo: Add Idle Animation with small movements instead of no animation?
		FProcessedAnimData step = ASS->AnimData[addAtStep];
		step.Timestamp += timePerStep;
		addAtStep++;
		addStep(step, addAtStep);
	}
}

void ARecordingModificator::deleteTimeOverlappingSteps()
{
	FTimespan lastTimeStemp = ASS->AnimData[0].Timestamp;
	for (int i = 1; i < ASS->AnimData.Num(); i++) {
		if (ASS->AnimData[i].Timestamp < lastTimeStemp) {
			deleteStep(i);
		}
	}
}

bool ARecordingModificator::stepContainsMeasurements(int step)
{
	if (step < 0 || step >= ASS->AnimData.Num()) {
		return false;
	}
	if (ASS->AnimData[step].IsEnd || ASS->AnimData[step].IsMarker) {
		return false;
	}
	return true;
}

FString ARecordingModificator::ArrayOfIntsToString(TArray<int> array)
{
	FString ret = "";
	if (array.Num() > 0) {
		int i = 0;
		ret += FString::FromInt(array[i]);
		i++;
		while (i < array.Num()) {
			ret += ", " + FString::FromInt(array[i]);
			i++;
		}

	}
	return ret;
}

float ARecordingModificator::getRotAngularDistance(FSensorData* step1, FSensorData* step2, EBodyPart bp)
{
	FQuat start = getBodyPartSensorData(bp, step1)->Rot;
	FQuat end = getBodyPartSensorData(bp, step2)->Rot;
	return abs(start.AngularDistance(end));
}

float ARecordingModificator::getPosDistance(FSensorData* step1, FSensorData* step2, EBodyPart bp)
{
	FVector start = getBodyPartSensorData(bp, step1)->Pos;
	FVector end = getBodyPartSensorData(bp, step2)->Pos;
	return abs(FVector::Distance(start, end));
}

float ARecordingModificator::getFingerDataDistance(FFingerData* step1, FFingerData* step2, EHandPart hp, bool rightHand)
{
	float start = getHandPartData(hp, rightHand, step1);
	float end = getHandPartData(hp, rightHand, step2);
	return abs(end - start);
}

FString ARecordingModificator::getBodyPartName(EBodyPart bp)
{
	switch (bp) {
	case EBodyPart::Head:
		return "Head";
	case EBodyPart::HandL:
		return "HandL";
	case EBodyPart::HandR:
		return "HandR";
	case EBodyPart::LowerArmL:
		return "LowerArmL";
	case EBodyPart::LowerArmR:
		return "LowerArmR";
	case EBodyPart::LowerLegL:
		return "LowerLegL";
	case EBodyPart::LowerLegR:
		return "LowerLegR";
	case EBodyPart::UpperBody:
		return "UpperBody";
	case EBodyPart::LowerBody:
		return "LowerBody";
	default:
		return "undefinded";
	}
}

FString ARecordingModificator::getHandPartName(EHandPart hp)
{
	switch (hp) {
		case EHandPart::Thumb: return "Thumb";
		case EHandPart::Index: return "Index";
		case EHandPart::Middle: return "Middle";
		case EHandPart::Ring: return "Ring";
		case EHandPart::Pinky: return "Pinky";
		case EHandPart::Thumb_Index: return "Thumb_Index";
		case EHandPart::Index_Middle: return "Index_Middle";
		case EHandPart::Middle_Ring: return "Middle_Ring";
		case EHandPart::Ring_Pinky: return "Ring_Pinky";
		default: return "undefinded";
	}
}

float ARecordingModificator::getHandPartData(EHandPart hp, bool rightHand, FFingerData* data)
{
	FFingerDataEntry* hand;
	if (rightHand) {
		hand = &data->RightHand;
	}
	else {
		hand = &data->LeftHand;
	}
	switch (hp) {
	case EHandPart::Thumb: return hand->Thumb;
	case EHandPart::Index: return hand->Index;
	case EHandPart::Middle: return hand->Middle;
	case EHandPart::Ring: return hand->Ring;
	case EHandPart::Pinky: return hand->Pinky;
	case EHandPart::Thumb_Index: return hand->Thumb_Index;
	case EHandPart::Index_Middle: return hand->Index_Middle;
	case EHandPart::Middle_Ring: return hand->Middle_Ring;
	case EHandPart::Ring_Pinky: return hand->Ring_Pinky;
	}
	return -1;
}

void ARecordingModificator::setHandPartData(EHandPart hp, bool rightHand, FFingerData* data, float value)
{
	FFingerDataEntry* hand;
	if (rightHand) {
		hand = &data->RightHand;
	}
	else {
		hand = &data->LeftHand;
	}
	switch (hp) {
	case EHandPart::Thumb: hand->Thumb = value; return;
	case EHandPart::Index: hand->Index = value; return;
	case EHandPart::Middle: hand->Middle = value; return;
	case EHandPart::Ring: hand->Ring = value; return;
	case EHandPart::Pinky: hand->Pinky = value; return;
	case EHandPart::Thumb_Index: hand->Thumb_Index = value; return;
	case EHandPart::Index_Middle: hand->Index_Middle = value; return;
	case EHandPart::Middle_Ring: hand->Middle_Ring = value; return;
	case EHandPart::Ring_Pinky: hand->Ring_Pinky = value; return;
	}
}

int ARecordingModificator::getAssSize()
{
	return ASS->AnimData.Num();
}

FTimespan ARecordingModificator::GetTime(int step)
{
	return ASS->AnimData[step].Timestamp;
}

float ARecordingModificator::getDeviation(int start, int length, bool body, bool rightHand, bool position, EBodyPart bp, EHandPart hp)
{
	if (body) {
		if (position) {
			return getPosDistance(&ASS->AnimData[start].SensorData, &ASS->AnimData[start + length].SensorData, bp);
		}
		else {
			return getRotAngularDistance(&ASS->AnimData[start].SensorData, &ASS->AnimData[start + length].SensorData, bp);
		}
	}
	else {
		return getFingerDataDistance(&ASS->AnimData[start].FingerData, &ASS->AnimData[start + length].FingerData, hp, rightHand);
	}
}

void ARecordingModificator::debugMsg(FString msg, FColor color)
{
	if (debugMsgs) {
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, color, msg);
	}
	UE_LOG(LogTemp, Display, TEXT("%s"), *msg);
}

void ARecordingModificator::SetTime(FTimespan t, int step)
{
	if (ASS) {
		if (TrackedModifications) {
			FManipulation manip;
			manip.Typ = FModType::TimeShift;
			manip.startStep = step;
			manip.endStep = step;
			manip.duration = t - ASS->AnimData[step].Timestamp;
			TrackedModifications->Add(manip);
		}
		ASS->AnimData[step].Timestamp = t;
	}
}

void ARecordingModificator::SetRot(EBodyPart bodyPart, FQuat RotQuat, int step)
{
	FQuat oldRot = getBodyPartSensorData(bodyPart, &ASS->AnimData[step].SensorData)->Rot;
	if (TrackedModifications) {
		FManipulation manip;
		manip.Typ = RotationShift;
		manip.startStep = step;
		manip.endStep = step;
		manip.bodyPartToShift = bodyPart;
		manip.rotQuat = RotQuat - oldRot;
		TrackedModifications->Add(manip);
	}
	getBodyPartSensorData(bodyPart, &ASS->AnimData[step].SensorData)->Rot = RotQuat;
}

FQuat ARecordingModificator::GetRot(EBodyPart bodyPart, int step)
{
	return getBodyPartSensorData(bodyPart, &ASS->AnimData[step].SensorData)->Rot;
}

void ARecordingModificator::SetPos(EBodyPart bodyPart, FVector PosVec, int step)
{
	FVector oldVec = getBodyPartSensorData(bodyPart, &ASS->AnimData[step].SensorData)->Pos;
	if (TrackedModifications) {
		FManipulation manip;
		manip.Typ = PositionShift;
		manip.startStep = step;
		manip.endStep = step;
		manip.bodyPartToShift = bodyPart;
		manip.posVec = PosVec - oldVec;
		TrackedModifications->Add(manip);
	}
	getBodyPartSensorData(bodyPart, &ASS->AnimData[step].SensorData)->Pos = PosVec;
}

FVector ARecordingModificator::GetPos(EBodyPart bodyPart, int step)
{
	return getBodyPartSensorData(bodyPart, &ASS->AnimData[step].SensorData)->Pos;
}

void ARecordingModificator::SetFingerData(EHandPart handPart, bool rightHand, float FingerValue, int step)
{
	if (TrackedModifications) {
		FManipulation manip;
		manip.Typ = FingerShift;
		manip.startStep = step;
		manip.endStep = step;
		manip.fingerMovement = FingerValue - getHandPartData(handPart, rightHand, &ASS->AnimData[step].FingerData);
		manip.rightHand = rightHand;
		manip.handPartToShift = handPart;
		TrackedModifications->Add(manip);
	}
	setHandPartData(handPart, rightHand, &ASS->AnimData[step].FingerData, FingerValue);
}

float ARecordingModificator::GetFingerData(EHandPart handPart, bool rightHand, int step)
{
	return getHandPartData(handPart, rightHand, &ASS->AnimData[step].FingerData);
}

void ARecordingModificator::copyStepWithDifferentTime(int posCopy, int posPaste, FTimespan time)
{
	FProcessedAnimData step = ASS->AnimData[posCopy];
	step.Timestamp = time;
	addStep(step, posPaste);
}

