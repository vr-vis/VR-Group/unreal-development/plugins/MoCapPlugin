// Fill out your copyright notice in the Description page of Project Settings.


#include "Modificators/RemovePeakRecordingModificator.h"

ARemovePeakRecordingModificator::ARemovePeakRecordingModificator()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void ARemovePeakRecordingModificator::BeginPlay()
{
	Super::BeginPlay();
}

void ARemovePeakRecordingModificator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (assSet && !Inited && ASS && ASS->AnimData.Num()>0) {
		setBodyPartLimits();
		setFingerPartLimits();
		Inited = true;
	}
}

void ARemovePeakRecordingModificator::SetAnimSaveState(FAnimSaveState* ass)
{
	assSet = false;
	originalASS = ass;
	modifiedASS = *ass;
	setASS();
	if (ASS->AnimData.Num() > 0) {
		assSet = true;
	}
}

void ARemovePeakRecordingModificator::ApplyModification()
{
	eliminateJumps();
}

void ARemovePeakRecordingModificator::showModification()
{
	if (!assSet || !Inited) {
		debugMsg("AnimationSaveState not Set yet -> No Mainpulation possible", FColor::White);
		return;
	}
	TArray<TArray<int>> allPosJumps = getPositionJumps();
	TArray<TArray<int>> allRotJumps = getRotationJumps();

	for (int j = 0; j < EBodyPart::LAST; j++) {
		EBodyPart bp = EBodyPart(j);
		TArray<int> jumpPositions = allPosJumps[j];
		debugMsg(FString::FromInt(jumpPositions.Num()) + " Position-Jumps found for " + getBodyPartName(bp) + ": " + ArrayOfIntsToString(jumpPositions), FColor::White);

		TArray<int> jumpRotations = allRotJumps[j];
		debugMsg(FString::FromInt(jumpPositions.Num()) + " Rotation-Jumps found for " + getBodyPartName(bp) + ": " + ArrayOfIntsToString(jumpRotations), FColor::White);
	}

	TArray<TArray<int>> allFingerJumpsRh = getFingerJumps(true);
	TArray<TArray<int>> allFingerJumpsLh = getFingerJumps(false);
	for (int j = 0; j < EHandPart::Hand_LAST; j++) {
		EHandPart hp = EHandPart(j);
		TArray<int> RightHandJumps = allFingerJumpsRh[j];
		debugMsg(FString::FromInt(RightHandJumps.Num()) + " Right-Hand-Jumps found for " + getHandPartName(hp) + ": " + ArrayOfIntsToString(RightHandJumps), FColor::White);
		TArray<int> LeftHandJumps = allFingerJumpsLh[j];
		debugMsg(FString::FromInt(LeftHandJumps.Num()) + " Left-Hand-Jumps found for " + getHandPartName(hp) + ": " + ArrayOfIntsToString(LeftHandJumps), FColor::White);
	}
}

void ARemovePeakRecordingModificator::setASS()
{
	if (workOnProductive) {
		ASS = originalASS;
	}
	else {
		ASS = &modifiedASS;
	}
}

void ARemovePeakRecordingModificator::copyStepWithDifferentTime(int posCopy, int posPaste, FTimespan time)
{
	if (!assSet || !Inited) {
		debugMsg("AnimationSaveState not Set yet -> No Mainpulation possible", FColor::White);
		return;
	}
	Super::copyStepWithDifferentTime(posCopy, posPaste, time);
}

TArray<int> ARemovePeakRecordingModificator::findPositionJumps(EBodyPart bp)
{
	TArray<int> ret;
	FSensorData* prevStep = &ASS->AnimData[0].SensorData;
	for (int i = 1; i < ASS->AnimData.Num(); i++) {
		FSensorData* currentStep = &ASS->AnimData[i].SensorData;
		if (stepContainsMeasurements(i - 1) && stepContainsMeasurements(i)) {
			float dist = getPosDistance(prevStep, currentStep, bp);
			if (dist > MaximalTranslationVectorLength.getBodyPartEntry(bp)) {
				ret.Add(i - 1);
			}
		}
		prevStep = currentStep;
	}
	return ret;
}

TArray<int> ARemovePeakRecordingModificator::findRotationJumps(EBodyPart bp)
{
	TArray<int> ret;
	FSensorData* prevStep = &ASS->AnimData[0].SensorData;
	for (int i = 1; i < ASS->AnimData.Num(); i++) {
		FSensorData* currentStep = &ASS->AnimData[i].SensorData;
		if (stepContainsMeasurements(i - 1) && stepContainsMeasurements(i)) {
			float dist = getRotAngularDistance(prevStep, currentStep, bp);
			if (dist > MaximalRotationAngle.getBodyPartEntry(bp)) {
				ret.Add(i - 1);
			}
		}
		prevStep = currentStep;
	}
	return ret;
}

TArray<int> ARemovePeakRecordingModificator::findFingerJumps(EHandPart hp, bool rightHand)
{
	TArray<int> ret;
	float prev = getHandPartData(hp, rightHand, &ASS->AnimData[0].FingerData);
	for (int i = 1; i < ASS->AnimData.Num(); i++) {
		float curr = getHandPartData(hp, rightHand, &ASS->AnimData[i].FingerData);
		if (stepContainsMeasurements(i - 1) && stepContainsMeasurements(i)) {
			float dist = abs(curr - prev);
			if ((dist > MaximalRightHandFingerMovementPerStep.getHandPartData(hp) && rightHand) 
				|| (dist > MaximalLeftHandFingerMovementPerStep.getHandPartData(hp) && !rightHand)) {
				ret.Add(i - 1);
			}
		}
		prev = curr;
	}
	return ret;
}


float ARemovePeakRecordingModificator::calculateMedian(TArray<float> a)
{
	a.Sort();
	int pos = a.Num() / 2;
	float ret = 0.0f;
	while (ret == 0.0f && pos < a.Num()) {
		ret = a[pos];
		pos++;
	}
	return ret;
}

void ARemovePeakRecordingModificator::setBodyPartLimits()
{
	if (SetMaximalRotationAngleManually && SetMaximalTranslationVectorLengthManually) {
		return;
	}
	for (int j = 0; j < EBodyPart::LAST; j++) {
		EBodyPart bp = EBodyPart(j);
		TArray<float> rot;
		TArray<float> pos;
		FQuat prevRot = getBodyPartSensorData(bp, &ASS->AnimData[0].SensorData)->Rot;
		FVector prevPos = getBodyPartSensorData(bp, &ASS->AnimData[0].SensorData)->Pos;
		for (int i = 1; i < ASS->AnimData.Num(); i++) {
			FVector curPos = getBodyPartSensorData(bp, &ASS->AnimData[i].SensorData)->Pos;
			FQuat curRot = getBodyPartSensorData(bp, &ASS->AnimData[i].SensorData)->Rot;
			pos.Add(FVector::Distance(prevPos, curPos));
			rot.Add(abs(prevRot.AngularDistance(curRot)));
			prevPos = curPos;
			prevRot = curRot;
		}
		if (!SetMaximalRotationAngleManually) {
			MaximalRotationAngle.setBodyPartEntry(bp, calculateMedian(rot) * FactorOfMedianForMaximalRotationAngle);
		}
		if (!SetMaximalTranslationVectorLengthManually) {
			MaximalTranslationVectorLength.setBodyPartEntry(bp, calculateMedian(pos) * FactorOfMedianForMaximalTranslationVectorLength);
		}
	}
}

void ARemovePeakRecordingModificator::setFingerPartLimits()
{
	if (SetMaxFingerMovementManually) {
		return;
	}
	for (int j = 0; j < EHandPart::Hand_LAST; j++) {
		EHandPart hp = EHandPart(j);
		TArray<float> leftHand;
		TArray<float> rightHand;
		float prevRH = getHandPartData(hp, true, &ASS->AnimData[0].FingerData);
		float prevLH = getHandPartData(hp, false, &ASS->AnimData[0].FingerData);

		for (int i = 1; i < ASS->AnimData.Num(); i++) {
			float currRH = getHandPartData(hp, true, &ASS->AnimData[i].FingerData);
			float currLH = getHandPartData(hp, false, &ASS->AnimData[i].FingerData);
			leftHand.Add(abs(currLH - prevLH));
			rightHand.Add(abs(currRH - prevRH));
			prevRH = currRH;
			prevLH = currLH;
		}
		float mR = calculateMedian(rightHand);
		float mL = calculateMedian(leftHand);
		MaximalRightHandFingerMovementPerStep.setHandPartData(hp, mR * FactorOfMedianForMaximalFingerMovement);
		MaximalLeftHandFingerMovementPerStep.setHandPartData(hp, mL * FactorOfMedianForMaximalFingerMovement);
	}
}

int ARemovePeakRecordingModificator::findRangeWithoutBPJumps(int JumpPos, int rangeLength, EBodyPart bp, int lengthJumpBlock)
{
	int maxBack = prepareStart(JumpPos + lengthJumpBlock - rangeLength);
	int maxFore = prepareEnd(JumpPos + rangeLength)-rangeLength;

	for (int i = maxBack; i <= maxFore; i++) {
		if (stepContainsMeasurements(i) && stepContainsMeasurements(i + rangeLength)) {
			if ((getPosDistance(&ASS->AnimData[i].SensorData, &ASS->AnimData[i + rangeLength].SensorData, bp) < MaximalTranslationVectorLength.getBodyPartEntry(bp) * rangeLength) 
			&& (getRotAngularDistance(&ASS->AnimData[i].SensorData, &ASS->AnimData[i + rangeLength].SensorData, bp) < MaximalRotationAngle.getBodyPartEntry(bp) * rangeLength)) {
				return i;
			}
		}
	}
	
	return -1;
}

int ARemovePeakRecordingModificator::findRangeWithoutFingerJumps(int JumpPos, int rangeLength, EHandPart hp, bool rightHand, int lengthJumpBlock)
{
	int maxBack = prepareStart(JumpPos + lengthJumpBlock - rangeLength);
	int maxFore = prepareEnd(JumpPos + rangeLength) - rangeLength;

	for (int i = maxBack; i <= maxFore; i++) {
		if (stepContainsMeasurements(i) && stepContainsMeasurements(i + rangeLength)) {
			if (abs(getHandPartData(hp, rightHand, &ASS->AnimData[i].FingerData) - getHandPartData(hp, rightHand, &ASS->AnimData[i + rangeLength].FingerData)) < (MaximalLeftHandFingerMovementPerStep.getHandPartData(hp) * (!rightHand) + MaximalRightHandFingerMovementPerStep.getHandPartData(hp) * rightHand) * rangeLength) {
				return i;
			}
		}
	}
	return -1;
}

TArray<FManipulation>* ARemovePeakRecordingModificator::getModification()
{
	showModification();
	return nullptr;
}

TArray<TArray<int>> ARemovePeakRecordingModificator::getPositionJumps()
{
	TArray<TArray<int>> ret;
	if (!assSet) {
		return ret;
	}
	for (int j = 0; j < EBodyPart::LAST; j++) {
		EBodyPart bp = EBodyPart(j);
		ret.Push(findPositionJumps(bp));
	}
	return ret;
}

TArray<TArray<int>> ARemovePeakRecordingModificator::getRotationJumps()
{
	TArray<TArray<int>> ret;
	if (!assSet) {
		return ret;
	}
	for (int j = 0; j < EBodyPart::LAST; j++) {
		EBodyPart bp = EBodyPart(j);
		ret.Push(findRotationJumps(bp));
	}
	return ret;
}

TArray<TArray<int>> ARemovePeakRecordingModificator::getFingerJumps(bool rightHand)
{
	TArray<TArray<int>> ret;
	if (!assSet) {
		return ret;
	}
	for (int j = 0; j < EHandPart::Hand_LAST; j++) {
		EHandPart hp = EHandPart(j);
		ret.Push(findFingerJumps(hp, rightHand));
	}
	return ret;
}

void ARemovePeakRecordingModificator::eliminateJumps()
{
	if (!assSet) {
		return;
	}
	if (smoothRotation) {
		eliminateBodypartJumps(getRotationJumps(), false);
	}
	if (smoothPosition) {
		eliminateBodypartJumps(getPositionJumps(), true);
	}
	if (smoothFinger) {
		eliminateFingerJumps(true);
		eliminateFingerJumps(false);
	}
}

FTimespan ARemovePeakRecordingModificator::getMaximalSliceLength()
{
	return MaximalSliceLength;
}

void ARemovePeakRecordingModificator::setMaximalSliceLength(FTimespan ts)
{
	MaximalSliceLength = ts;
}

FTimespan ARemovePeakRecordingModificator::getTimespanFromTicks(int Ticks)
{
	return FTimespan(Ticks);
}

int ARemovePeakRecordingModificator::getTicksFromTimespan(FTimespan ts)
{
	return ts.GetTicks();
}

void ARemovePeakRecordingModificator::reloadParams()
{
	Inited = false;
}

void ARemovePeakRecordingModificator::eliminateBodypartJumps(TArray<TArray<int>> jumps, bool position)
{
	for (int j = 0; j < jumps.Num(); j++) {
		EBodyPart bp = EBodyPart(j);
		for (int i = 0; i < jumps[j].Num(); i++) {
			int pos = jumps[j][i];
			int length = 1;
			while (jumps[j].Num()>i + length && jumps[j][i + length] == pos + length) {
				length++;
			}

			TTuple<int, int> candidate = getBestPositionAndLengthForSmoothing(pos, length, true, false, position, bp, EHandPart::Hand_LAST);
			if (candidate.Key != -1) {
				smoothOneBodyPartData(candidate.Key, candidate.Value, bp);
				i += length - 1;
			}
		}
	}
}

void ARemovePeakRecordingModificator::eliminateFingerJumps(bool rightHand)
{
	TArray<TArray<int>> jumps = getFingerJumps(rightHand);
	for (int j = 0; j < jumps.Num(); j++) {
		EHandPart hp = EHandPart(j);
		for (int i = 0; i < jumps[j].Num(); i++) {
			int pos = jumps[j][i];
			int length = 1;
			while (jumps[j].Num() > i + length && jumps[j][i + length] == pos + length) {
				length++;
			}
			TTuple<int, int> candidate = getBestPositionAndLengthForSmoothing(pos, length, false, rightHand, false, EBodyPart::LAST, hp);
			if (candidate.Key != -1) {
				smoothOneFingerData(candidate.Key, candidate.Value, hp, rightHand);
				i += length - 1;
			}
		}
	}
}

void ARemovePeakRecordingModificator::smoothOneBodyPartData(int start, int length, EBodyPart bp)
{
	FQuat rotBefore = getBodyPartSensorData(bp, &ASS->AnimData[start].SensorData)->Rot.GetNormalized();
	FVector rotStart = getBodyPartSensorData(bp, &ASS->AnimData[start].SensorData)->Rot.GetRotationAxis();
	FVector rotAfter = getBodyPartSensorData(bp, &ASS->AnimData[start + length].SensorData)->Rot.GetRotationAxis();
	FVector rotstep = (rotAfter - rotStart) / length;
	for (int k = 1; k < length; k++) {
		rotBefore.RotateVector(rotstep);
		SetRot(bp, rotBefore, start + k);
	}

	FVector posBefore = getBodyPartSensorData(bp, &ASS->AnimData[start].SensorData)->Pos;
	FVector posAfter = getBodyPartSensorData(bp, &ASS->AnimData[start + length].SensorData)->Pos;
	FVector posstep = (posAfter - posBefore) / length;
	for (int k = 1; k < length; k++) {
		posBefore += posstep;
		SetPos(bp, posBefore, start + k);
	}
	FString msg = ("Did Smoothing in " + getBodyPartName(bp) + " step " + FString::FromInt(start) + " to " + FString::FromInt(start + length));
	debugMsg(msg, FColor::Yellow);
}

void ARemovePeakRecordingModificator::smoothOneFingerData(int start, int length, EHandPart hp, bool rightHand)
{
	float before = getHandPartData(hp, rightHand, &ASS->AnimData[start].FingerData);
	float after = getHandPartData(hp, rightHand, &ASS->AnimData[start+length].FingerData);
	float step = (after - before) / length;
	for (int k = 1; k < length; k++) {
		before += step;
		SetFingerData(hp, rightHand, before, start + k);
	}
	if (debugMsgs) {
		FString hand = "Left";
		if (rightHand) {
			hand = "Right";
		}
		FString msg = ("Did Smoothing in " + getHandPartName(hp) + " of the " + hand + " Hand in steps " + FString::FromInt(start) + " to " + FString::FromInt(start + length));
		debugMsg(msg, FColor::Yellow);
	}
}

float ARemovePeakRecordingModificator::calculateCost(int length, float wholeDeviation, float startDeviation, float endDeviation)
{
	float costStart = startDeviation * stepSizeCost;
	float costEnd = endDeviation * stepSizeCost;
	float costSteps = (stepCost * length);
	float costStepSize = (wholeDeviation / length * stepSizeCost);
	return costStart + costEnd + costSteps + costStepSize;
}

TTuple<int, int> ARemovePeakRecordingModificator::getBestPositionAndLengthForSmoothing(int JumpPos, int length, bool body, bool rightHand, bool position, EBodyPart bp, EHandPart hp)
{
	TMap<float, TTuple<int, int>> candidates;
	TArray<float> keys;
	for (int addLength = 0; addLength * getTimePerStep() <= MaximalSliceLength; addLength++) {
		int fullLength = length + addLength;
		int maxBack = prepareStart(JumpPos - addLength);
		int maxFore = prepareEnd(JumpPos + fullLength) - fullLength;

		for (int i = maxBack; i <= maxFore; i++) {
			if (stepContainsMeasurements(i) && stepContainsMeasurements(i + length)) {
				float deviation = getDeviation(i, fullLength, body, rightHand, position, bp, hp);
				if (checkDeviation(deviation/fullLength, body, rightHand, position, bp, hp)) {
					float StartDeviation = 0;
					float EndDeviation = 0;
					if (useStartAndEndJumpsForCosts && i > 0) {
						StartDeviation = getDeviation(i - 1, 1, body, rightHand, position, bp, hp);
					}
					if (useStartAndEndJumpsForCosts && i < ASS->AnimData.Num()  - 1) {
						EndDeviation = getDeviation(i, 1, body, rightHand, position, bp, hp);
					}
					float cost = calculateCost(fullLength, deviation, StartDeviation, EndDeviation);
					candidates.Add(cost, TTuple<int, int>(i, fullLength));
					keys.Add(cost);
				}
			}
		}
	}
	if (candidates.Num() == 0) {
		return TTuple<int, int>(-1, -1);
	}

	keys.Sort();
	return (candidates[keys[0]]);
}

bool ARemovePeakRecordingModificator::checkDeviation(float deviation, bool body, bool rightHand, bool position, EBodyPart bp, EHandPart hp)
{
	if (body) {
		if (position) {
			return MaximalTranslationVectorLength.getBodyPartEntry(bp) >= deviation;
		}
		else {
			return MaximalRotationAngle.getBodyPartEntry(bp) >= deviation;
		}
	}
	else {
		if (rightHand) {
			return MaximalRightHandFingerMovementPerStep.getHandPartData(hp) >= deviation;
		}
		else {
			return MaximalLeftHandFingerMovementPerStep.getHandPartData(hp) >= deviation;
		}
	}
	return false;
}

void ARemovePeakRecordingModificator::unsetAss()
{
	assSet = false;
}
