// Fill out your copyright notice in the Description page of Project Settings.


#include "Modificators/ManuCreateRecordingModificator.h"

AManuCreateRecordingModificator::AManuCreateRecordingModificator()
{
}

void AManuCreateRecordingModificator::BeginPlay()
{
	Super::BeginPlay();
}

void AManuCreateRecordingModificator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AManuCreateRecordingModificator::ApplyModification()
{
	if (setManualManipulations) {
		ExecuteMaipulations(&ManipulationList);
	}
	else {
		GenerateAndExecuteManipulations(Start, End, MinLength, MaxLength, bodyPartsToManipulate, NumberOfManipulations);
	}
}

void AManuCreateRecordingModificator::GenerateAndExecuteManipulations(int start, int end, int minLength, int maxLength, TMap<FString, bool> bodyParts, int numberOfManipulations)
{
	TArray<FManipulation> manips = generateManipulations(start, end, minLength, maxLength, bodyParts, numberOfManipulations);
	ExecuteMaipulations(&manips);
}

TArray<FManipulation> AManuCreateRecordingModificator::generateManipulations(int start, int end, int minLength, int maxLength, TMap<FString, bool> bodyParts, int numberOfManipulations)
{
	TArray<FManipulation> ret;
	for (int i = 0; i < numberOfManipulations; i++) {
		ret.Add(generateOneManipulation(start, end, minLength, maxLength, bodyParts));
	}
	return ret;
}

FManipulation AManuCreateRecordingModificator::generateOneManipulation(int start, int end, int minLength, int maxLength, TMap<FString, bool> bodyParts)
{
	if (!manipulateRotations && !manipulatePositions) {
		return FManipulation();
	}
	if (!manipulateRotations) {
		return generatePositionManipulation(start, end, minLength, maxLength, bodyParts);
	}
	if (!manipulatePositions || FMath::RandRange(0, 1)) {
		return generateRotationManipulation(start, end, minLength, maxLength, bodyParts);
	}
	return generatePositionManipulation(start, end, minLength, maxLength, bodyParts);
}

FManipulation AManuCreateRecordingModificator::generatePositionManipulation(int start, int end, int minLength, int maxLength, TMap<FString, bool> bodyParts)
{
	if (!manipulatePositions) {
		return FManipulation();
	}
	FManipulation ret; 
	setPosLengthBodyPart(&ret, start, end, minLength, maxLength, bodyParts);
	ret.Typ = FModType::PositionShift;

	FVector vec = FVector(FMath::RandRange(-1.0f, 1.0), FMath::RandRange(-1.0f, 1.0), FMath::RandRange(-1.0f, 1.0));
	vec.Normalize();
	vec *= FMath::RandRange(minVectorLength, maxVectorLength);
	ret.posVec = vec;
	return ret;
}

FManipulation AManuCreateRecordingModificator::generateRotationManipulation(int start, int end, int minLength, int maxLength, TMap<FString, bool> bodyParts)
{
	if (!manipulateRotations) {
		return FManipulation();
	}
	FManipulation ret;
	setPosLengthBodyPart(&ret, start, end, minLength, maxLength, bodyParts);
	ret.Typ = FModType::RotationShift;

	FQuat quat;
	float max = 1;
	float min = -1;
	do {
		quat = FQuat(FMath::RandRange(min, max), FMath::RandRange(min, max), FMath::RandRange(min, max), FMath::RandRange(min, max));
		max += 1;
		min -= 1;
	} while (quat.GetAngle() > maxAngle);
	ret.rotQuat = quat;
	return ret;
}

void AManuCreateRecordingModificator::setPosLengthBodyPart(FManipulation* mani, int start, int end, int minLength, int maxLength, TMap<FString, bool> bodyParts)
{
	int maniLength = FMath::RandRange(minLength, maxLength);
	int maniPosition = FMath::RandRange(start, end - maniLength);
	if (end - maniLength <= start) {
		maniPosition = start;
	}
	mani->startStep = maniPosition;
	mani->endStep = maniPosition + maniLength;
	TArray<EBodyPart> bps;
	for (int j = 0; j < EBodyPart::LAST; j++) {
		EBodyPart bp = EBodyPart(j);
		if(bodyParts[getBodyPartName(bp)]){
			bps.Add(bp);
		}
	}
	if (bps.Num() <= 0) {
		return;
	}
	EBodyPart bp = bps[FMath::RandRange(0, bps.Num() - 1)];
	mani->bodyPartToShift = bp;
	return;
}

TMap<FString, bool> AManuCreateRecordingModificator::getBodyPartsToManipulate()
{
	return bodyPartsToManipulate;
}

void AManuCreateRecordingModificator::setBodyPartsToManipulate(TMap<FString, bool> bptm)
{
	bodyPartsToManipulate = bptm;
}

void AManuCreateRecordingModificator::addSteps(int position, FTimespan timeLength)
{
	if (!idleForTimeShift) {
		Super::addSteps(position, timeLength);
	}
	else {
		if (maxStepsForIdleAnim <= 0) {
			Super::addSteps(position, timeLength);
			return;
		}
		FTimespan startTime = ASS->AnimData[position].Timestamp;
		FTimespan timePerStep = getTimePerStep();
		int NumberOfSteps = timeLength.GetTicks() / timePerStep.GetTicks();
		int StepsDone = 0;
		bool upwards = false;
		//Set possible Range up/downwards
		int rangeUp = maxStepsForIdleAnim;
		if (position + rangeUp >= ASS->AnimData.Num()) {
			rangeUp = ASS->AnimData.Num() - position - 1;
		}
		int rangeDown = maxStepsForIdleAnim;
		if (position - rangeDown < 0) {
			rangeDown = position;
		}

		//Many Steps left -> go as far as allowed with copying
		while (NumberOfSteps - StepsDone > 2 * rangeDown + upwards * (-2 * rangeDown + 2 * rangeUp)) {
			int randUp = FMath::FRandRange(0, rangeUp);
			int randDown = FMath::FRandRange(0, rangeDown);
			for (int i = 1; i < randDown + upwards * (-1 * randDown + randUp); i++) {
				copyStepWithDifferentTime(position - i + upwards * (2 * i + StepsDone), position + StepsDone, startTime + timePerStep * StepsDone);
				StepsDone++;
			}
			for (int i = randDown + upwards * (-1 * randDown + randUp); i >= 0; i--) {
				copyStepWithDifferentTime(position - i + upwards * (2 * i + StepsDone), position + StepsDone, startTime + timePerStep * StepsDone);
				StepsDone++;
			}
			upwards = !upwards;
		}
		//Less Steps left -> go half the way of Steps left with copying and than back with the other half
		int halfOfRemaining = (NumberOfSteps - StepsDone) / 2;
		for (int i = 1; i < halfOfRemaining; i++) {
			copyStepWithDifferentTime(position - i + upwards * (2 * i + StepsDone), position + StepsDone, startTime + timePerStep * StepsDone);
			StepsDone++;
		}
		for (int i = halfOfRemaining; i >= 0; i--) {
			copyStepWithDifferentTime(position - i + upwards * (2 * i + StepsDone), position + StepsDone, startTime + timePerStep * StepsDone);
			StepsDone++;
		}
		//There may be one Step missing because of integer rounding
		while (StepsDone < NumberOfSteps) {
			copyStepWithDifferentTime(position, position + StepsDone, startTime + timePerStep * StepsDone);
			StepsDone++;
		}
	}
}
