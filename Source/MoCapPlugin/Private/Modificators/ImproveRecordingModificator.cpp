// Fill out your copyright notice in the Description page of Project Settings.


#include "Modificators/ImproveRecordingModificator.h"


AImproveRecordingModificator::AImproveRecordingModificator()
{
}

void AImproveRecordingModificator::BeginPlay()
{
	Super::BeginPlay();
}

void AImproveRecordingModificator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AImproveRecordingModificator::setPartsToModify(bool pos, bool rot, bool finger)
{
	smoothPosition = pos;
	smoothRotation = rot;
	smoothFinger = finger;
}

bool AImproveRecordingModificator::getPartsToModify(bool pos, bool rot, bool finger)
{
	if (pos) {
		return smoothPosition;
	}
	if (rot) {
		return smoothRotation;
	}
	return smoothFinger;
}
