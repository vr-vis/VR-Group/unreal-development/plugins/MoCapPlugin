// Fill out your copyright notice in the Description page of Project Settings.


#include "Modificators/TimeRecordingModificator.h"

ATimeRecordingModificator::ATimeRecordingModificator()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	// PrimaryActorTick.bCanEverTick = true;
}

void ATimeRecordingModificator::BeginPlay()
{
	Super::BeginPlay();
}

void ATimeRecordingModificator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ATimeRecordingModificator::ManipulateCurrentFile()
{
	scaleTimeRandomly(StartStep, EndStep);
}

void ATimeRecordingModificator::ApplyModification()
{
	ManipulateCurrentFile();
}

void ATimeRecordingModificator::scaleTimeRandomly(int stepStart, int stepEnd)
{
	int start = prepareStart(stepStart);
	int end = prepareEnd(stepEnd);
	int pos = start;
	for (int i = start; i < end; i++) {
		if (stepContainsMeasurements(pos)) {
			int duplications = FMath::FRandRange(0, MaxScale);
			//FTimespan range = ASS->AnimData[pos + 1].Timestamp - ASS->AnimData[pos + 1].Timestamp;
			for (int j = 0; j < duplications; j++) {
				duplicateStep(pos);
				pos++;
				//ShiftTime(range / duplications, pos, pos);
			}
		}
		pos++;
	}
	distibuteTimeEqually();
}

int ATimeRecordingModificator::getMaxScale()
{
	return MaxScale;
}

void ATimeRecordingModificator::setMaxScale(int maxScale)
{
	MaxScale = maxScale;
}

int ATimeRecordingModificator::getStartStep()
{
	return prepareStart(StartStep);
}

void ATimeRecordingModificator::setStartStep(int startStep)
{
	StartStep = prepareStart(startStep);
}

int ATimeRecordingModificator::getEndStep()
{
	return prepareEnd(EndStep);
}

void ATimeRecordingModificator::setEndStep(int endStep)
{
	EndStep = prepareEnd(endStep);
}

void ATimeRecordingModificator::distibuteTimeEqually()
{
	FTimespan timePerStep = getTimePerStep();
	FTimespan startTime = ASS->AnimData[0].Timestamp;
	for (int i = 1; i < ASS->AnimData.Num()-1; i++) {
		SetTime(startTime + (i * timePerStep), i);
	}
}

