// Fill out your copyright notice in the Description page of Project Settings.


#include "Modificators/SmoothRecordingModificator.h"

ASmoothRecordingModificator::ASmoothRecordingModificator()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

void ASmoothRecordingModificator::BeginPlay()
{
	Super::BeginPlay();
}

void ASmoothRecordingModificator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

FQuat ASmoothRecordingModificator::smoothQuaternion(TArray<FQuat> quats)
{
	/*for (int i = 0; i < quats.Num(); i++) {
		quats[i].Normalize();
	}
	FQuat ret = FQuat(0, 0, 0, 0);
	for (int i = 0; i < quats.Num(); i++) {
		FQuat q = quats[i];
		int weight = 1;
		// Correct for double cover, by ensuring that dot product
		// of quats[i] and quats[0] is positive
		if ((quats[i] | quats[0]) < 0.0) {
			weight = -1;
		}
		ret += q;
	}
	return ret.GetNormalized();*/
	//https://math.stackexchange.com/questions/61146/averaging-quaternions
	if (quats.Num() < 1) {
		return FQuat(0, 0, 0, 0);
	}
	FQuat ret = quats[0];
	for (int i = 1; i < quats.Num(); i++) {
		float factor = 1.0 / (i + 1);
		ret = FQuat::Slerp(ret, quats[i], factor);
	}
	return ret.GetNormalized();
}

FVector ASmoothRecordingModificator::smoothVectors(TArray<FVector> vecs)
{
	FVector ret = FVector(0,0,0);
	for (int i = 0; i < vecs.Num(); i++) {
		ret += vecs[i];
	}
	return ret / vecs.Num();
}

float ASmoothRecordingModificator::smoothFloat(TArray<float> values)
{
	float ret = 0;
	for (int i = 0; i < values.Num(); i++) {
		ret += values[i];
	}
	return ret/values.Num();
}

TArray<FQuat> ASmoothRecordingModificator::getRotList(EBodyPart bp, int step)
{
	TArray<FQuat> ret;
	int start = prepareStart(step - (SmoothWindowSize/2));
	int end = prepareEnd(step + (SmoothWindowSize / 2));
	for (int i = start; i <= end; i++) {
		if (stepContainsMeasurements(i)) {
			ret.Add(getBodyPartSensorData(bp, &ASS->AnimData[i].SensorData)->Rot);
		}
	}
	return ret;
}

TArray<FVector> ASmoothRecordingModificator::getPosList(EBodyPart bp, int step)
{
	TArray<FVector> ret;
	int start = prepareStart(step - (SmoothWindowSize / 2));
	int end = prepareEnd(step + (SmoothWindowSize / 2));
	for (int i = start; i <= end; i++) {
		if (stepContainsMeasurements(i)) {
			ret.Add(getBodyPartSensorData(bp, &ASS->AnimData[i].SensorData)->Pos);
		}
	}
	return ret;
}

TArray<float> ASmoothRecordingModificator::getFingerList(EHandPart hp, bool rightHand, int step)
{
	TArray<float> ret;
	int start = prepareStart(step - (SmoothWindowSize / 2));
	int end = prepareEnd(step + (SmoothWindowSize / 2));
	for (int i = start; i <= end; i++) {
		if (stepContainsMeasurements(i)) {
			ret.Add(getHandPartData(hp, rightHand, &ASS->AnimData[i].FingerData));
		}
	}
	return ret;
}

void ASmoothRecordingModificator::smoothRotations()
{
	for (int j = 0; j < EBodyPart::LAST; j++) {
		EBodyPart bp = EBodyPart(j);
		TArray<FQuat> newQuats;
		for (int i = 0; i < ASS->AnimData.Num(); i++) {
			if (stepContainsMeasurements(i)) {
				newQuats.Add(smoothQuaternion(getRotList(bp, i)));
			}
			else {
				newQuats.Add(FQuat());
			}
		}
		//set new values
		for (int i = 0; i < ASS->AnimData.Num(); i++) {
			if (stepContainsMeasurements(i)) {
				SetRot(bp, newQuats[i], i);
			}
		}
	}
}

void ASmoothRecordingModificator::smoothPositions()
{
	for (int j = 0; j < EBodyPart::LAST; j++) {
		EBodyPart bp = EBodyPart(j);
		TArray<FVector> newPositions;
		for (int i = 0; i < ASS->AnimData.Num(); i++) {
			if (stepContainsMeasurements(i)) {
				newPositions.Add(smoothVectors(getPosList(bp, i)));
			}
			else {
				newPositions.Add(FVector());
			}
		}
		//set new values
		for (int i = 0; i < ASS->AnimData.Num(); i++) {
			if (stepContainsMeasurements(i)) {
				SetPos(bp, newPositions[i], i);
			}
		}
	}
}

void ASmoothRecordingModificator::smoothFingerData(bool rightHand)
{
	for (int j = 0; j < EHandPart::Hand_LAST; j++) {
		EHandPart hp = EHandPart(j);
		TArray<float> newFingerData;
		for (int i = 0; i < ASS->AnimData.Num(); i++) {
			if (stepContainsMeasurements(i)) {
				newFingerData.Add(smoothFloat(getFingerList(hp, rightHand, i)));
			}
			else {
				newFingerData.Add(0.0);
			}
		}
		//set new values
		for (int i = 0; i < ASS->AnimData.Num(); i++) {
			if (stepContainsMeasurements(i)) {
				SetFingerData(hp, rightHand, newFingerData[i], i);
			}
		}
	}
}

void ASmoothRecordingModificator::ApplyModification()
{
	smooth();
}

void ASmoothRecordingModificator::smooth()
{
	if (smoothRotation) {
		smoothRotations();
	}
	if (smoothPosition) {
		smoothPositions();
	}
	if (smoothFinger) {
		smoothFingerData(true);
		smoothFingerData(false);
	}
}

int ASmoothRecordingModificator::getSmoothWindowSize()
{
	return SmoothWindowSize;
}

void ASmoothRecordingModificator::setSmoothWindowSize(int value)
{
	SmoothWindowSize = value;
}

