// Fill out your copyright notice in the Description page of Project Settings.


#include "MCRigUnits.h"
#include "Units/RigUnitContext.h"
#include "Math/ControlRigMathLibrary.h"

FRigUnit_SensorOffsets::FRigUnit_SensorOffsets() {
	ControlTrans = FTransform::Identity;
	SensorOffset = FSensorOffset();
	OutTrans = FTransform::Identity;
}

FRigUnit_SensorOffsets_Execute() {
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_RIGUNIT()

	//NewControl = Bone * Control(-1) * NewControl
	OutTrans = SensorOffset.DeltaTransform * ControlTrans;

}

FRigUnit_ApplyFingerData::FRigUnit_ApplyFingerData() {
	
}

void AddRotation(const FName& BoneName, float Angle, FRigBoneHierarchy* Hierarchy, const FName& Axis = "Y") {
	if (Axis == "X") {
		Hierarchy->SetLocalTransform(BoneName, FTransform(Hierarchy->GetLocalTransform(BoneName).GetRotation().Rotator().Add(0, 0, -Angle), Hierarchy->GetLocalTransform(BoneName).GetTranslation()));
	}
	else if (Axis == "Y") {
		Hierarchy->SetLocalTransform(BoneName, FTransform(Hierarchy->GetLocalTransform(BoneName).GetRotation().Rotator().Add(0, -Angle, 0), Hierarchy->GetLocalTransform(BoneName).GetTranslation()));
	}
	else if (Axis == "Z") {
		Hierarchy->SetLocalTransform(BoneName, FTransform(Hierarchy->GetLocalTransform(BoneName).GetRotation().Rotator().Add(-Angle, 0, 0), Hierarchy->GetLocalTransform(BoneName).GetTranslation()));
	}
}

FRigUnit_ApplyFingerData_Execute() {
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_RIGUNIT()

	FVector Scale(AngleScale, 0.7f * AngleScale, 0.4f * AngleScale);
	FVector ThumbScale(AngleScale, AngleScale, AngleScale);
	if (AngleScale >= 1.f) {
		Scale = FVector(1.f, 1.f, 1.f);
		ThumbScale = FVector(1.f, 1.f, 1.f);
	}
	FVector ThumbAnglesClosed_N = ThumbScale * ThumbAnglesClosed;
	FVector IndexAnglesClosed_N = Scale * IndexAnglesClosed;
	FVector MiddleAnglesClosed_N = Scale * MiddleAnglesClosed;
	FVector RingAnglesClosed_N = Scale * RingAnglesClosed;
	FVector PinkyAnglesClosed_N = Scale * PinkyAnglesClosed;
	FVector ThumbAnglesOpen_N = ThumbAnglesOpen;
	FVector IndexAnglesOpen_N = IndexAnglesOpen;
	FVector MiddleAnglesOpen_N = MiddleAnglesOpen;
	FVector RingAnglesOpen_N = RingAnglesOpen;
	FVector PinkyAnglesOpen_N = PinkyAnglesOpen;
	
	float Thumb_Index_Angle_N = Thumb_Index_Angle;
	float Index_Middle_Angle_N = Index_Middle_Angle;
	float Middle_Ring_Angle_N = Middle_Ring_Angle;
	float Ring_Pinky_Angle_N = Ring_Pinky_Angle;

	float ReverseFactor = 0.5f;
	FFingerData FingerDataTransformed = FingerData;
	FingerDataTransformed.LeftHand.Thumb = FingerDataTransformed.LeftHand.Thumb * (1.f + ReverseFactor) - ReverseFactor;
	FingerDataTransformed.LeftHand.Index = FingerDataTransformed.LeftHand.Index * (1.f + ReverseFactor) - ReverseFactor;
	FingerDataTransformed.LeftHand.Middle = FingerDataTransformed.LeftHand.Middle * (1.f + ReverseFactor) - ReverseFactor;
	FingerDataTransformed.LeftHand.Ring = FingerDataTransformed.LeftHand.Ring * (1.f + ReverseFactor) - ReverseFactor;
	FingerDataTransformed.LeftHand.Pinky = FingerDataTransformed.LeftHand.Pinky * (1.f + ReverseFactor) - ReverseFactor;
	FingerDataTransformed.RightHand.Thumb = FingerDataTransformed.RightHand.Thumb * (1.f + ReverseFactor) - ReverseFactor;
	FingerDataTransformed.RightHand.Index = FingerDataTransformed.RightHand.Index * (1.f + ReverseFactor) - ReverseFactor;
	FingerDataTransformed.RightHand.Middle = FingerDataTransformed.RightHand.Middle * (1.f + ReverseFactor) - ReverseFactor;
	FingerDataTransformed.RightHand.Ring = FingerDataTransformed.RightHand.Ring * (1.f + ReverseFactor) - ReverseFactor;
	FingerDataTransformed.RightHand.Pinky = FingerDataTransformed.RightHand.Pinky * (1.f + ReverseFactor) - ReverseFactor;

	FRigBoneHierarchy* Hierarchy = ExecuteContext.GetBones();
	
	if (Hierarchy) {

		//CURLS

		float Thumb01YAngleClosed = ThumbAnglesClosed_N.X;
		if (AngleScale >= 1.f) {
			Thumb01YAngleClosed = -15.f;
		}

		if (FingerDataTransformed.LeftHand.Thumb >= 0.f) {
			AddRotation("thumb_01_l", FingerDataTransformed.LeftHand.Thumb * Thumb01YAngleClosed, Hierarchy);
			AddRotation("thumb_01_l", FingerDataTransformed.LeftHand.Thumb * ThumbAnglesClosed_N.X, Hierarchy, "Z");
			AddRotation("thumb_02_l", FingerDataTransformed.LeftHand.Thumb * ThumbAnglesClosed_N.Y, Hierarchy);
			AddRotation("thumb_03_l", FingerDataTransformed.LeftHand.Thumb * ThumbAnglesClosed_N.Z, Hierarchy);
		}
		else {
			//AddRotation("thumb_01_l", FingerDataTransformed.LeftHand.Thumb * Thumb01YAngleClosed, Hierarchy);
			//AddRotation("thumb_01_l", (FingerDataTransformed.LeftHand.Thumb / ReverseFactor) * -ThumbAnglesOpen_N.X, Hierarchy, "X");
			AddRotation("thumb_01_l", (FingerDataTransformed.LeftHand.Thumb / ReverseFactor) * -ThumbAnglesOpen_N.X, Hierarchy);
			AddRotation("thumb_01_l", (FingerDataTransformed.LeftHand.Thumb / ReverseFactor) * ThumbAnglesOpen_N.X, Hierarchy, "Z");
			AddRotation("thumb_02_l", (FingerDataTransformed.LeftHand.Thumb / ReverseFactor) * ThumbAnglesOpen_N.Y, Hierarchy);
			AddRotation("thumb_03_l", (FingerDataTransformed.LeftHand.Thumb / ReverseFactor) * ThumbAnglesOpen_N.Z, Hierarchy);
		}

		if (FingerDataTransformed.LeftHand.Index >= 0.f) {
			AddRotation("index_01_l", FingerDataTransformed.LeftHand.Index * IndexAnglesClosed_N.X, Hierarchy);
			AddRotation("index_02_l", FingerDataTransformed.LeftHand.Index * IndexAnglesClosed_N.Y, Hierarchy);
			AddRotation("index_03_l", FingerDataTransformed.LeftHand.Index * IndexAnglesClosed_N.Z, Hierarchy);
		}
		else {
			AddRotation("index_01_l", FingerDataTransformed.LeftHand.Index * IndexAnglesOpen_N.X / ReverseFactor, Hierarchy);
			AddRotation("index_02_l", FingerDataTransformed.LeftHand.Index * IndexAnglesOpen_N.Y / ReverseFactor, Hierarchy);
			AddRotation("index_03_l", FingerDataTransformed.LeftHand.Index * IndexAnglesOpen_N.Z / ReverseFactor, Hierarchy);
		}

		if (FingerDataTransformed.LeftHand.Middle >= 0.f) {
			AddRotation("middle_01_l", FingerDataTransformed.LeftHand.Middle * MiddleAnglesClosed_N.X, Hierarchy);
			AddRotation("middle_02_l", FingerDataTransformed.LeftHand.Middle * MiddleAnglesClosed_N.Y, Hierarchy);
			AddRotation("middle_03_l", FingerDataTransformed.LeftHand.Middle * MiddleAnglesClosed_N.Z, Hierarchy);
		}
		else {
			AddRotation("middle_01_l", FingerDataTransformed.LeftHand.Middle * MiddleAnglesOpen_N.X / ReverseFactor, Hierarchy);
			AddRotation("middle_02_l", FingerDataTransformed.LeftHand.Middle * MiddleAnglesOpen_N.Y / ReverseFactor, Hierarchy);
			AddRotation("middle_03_l", FingerDataTransformed.LeftHand.Middle * MiddleAnglesOpen_N.Z / ReverseFactor, Hierarchy);
		}

		if (FingerDataTransformed.LeftHand.Ring >= 0.f) {
			AddRotation("ring_01_l", FingerDataTransformed.LeftHand.Ring * RingAnglesClosed_N.X, Hierarchy);
			AddRotation("ring_02_l", FingerDataTransformed.LeftHand.Ring * RingAnglesClosed_N.Y, Hierarchy);
			AddRotation("ring_03_l", FingerDataTransformed.LeftHand.Ring * RingAnglesClosed_N.Z, Hierarchy);
		}
		else {
			AddRotation("ring_01_l", FingerDataTransformed.LeftHand.Ring * RingAnglesOpen_N.X / ReverseFactor, Hierarchy);
			AddRotation("ring_02_l", FingerDataTransformed.LeftHand.Ring * RingAnglesOpen_N.Y / ReverseFactor, Hierarchy);
			AddRotation("ring_03_l", FingerDataTransformed.LeftHand.Ring * RingAnglesOpen_N.Z / ReverseFactor, Hierarchy);
		}

		if (FingerDataTransformed.LeftHand.Pinky >= 0.f) {
			AddRotation("pinky_01_l", FingerDataTransformed.LeftHand.Pinky * PinkyAnglesClosed_N.X, Hierarchy);
			AddRotation("pinky_02_l", FingerDataTransformed.LeftHand.Pinky * PinkyAnglesClosed_N.Y, Hierarchy);
			AddRotation("pinky_03_l", FingerDataTransformed.LeftHand.Pinky * PinkyAnglesClosed_N.Z, Hierarchy);
		}
		else {
			AddRotation("pinky_01_l", FingerDataTransformed.LeftHand.Pinky * PinkyAnglesOpen_N.X / ReverseFactor, Hierarchy);
			AddRotation("pinky_02_l", FingerDataTransformed.LeftHand.Pinky * PinkyAnglesOpen_N.Y / ReverseFactor, Hierarchy);
			AddRotation("pinky_03_l", FingerDataTransformed.LeftHand.Pinky * PinkyAnglesOpen_N.Z / ReverseFactor, Hierarchy);
		}

		if (FingerDataTransformed.RightHand.Thumb >= 0.f) {
			AddRotation("thumb_01_r", FingerDataTransformed.RightHand.Thumb * Thumb01YAngleClosed, Hierarchy);
			AddRotation("thumb_01_r", FingerDataTransformed.RightHand.Thumb * ThumbAnglesClosed_N.X, Hierarchy, "Z");
			AddRotation("thumb_02_r", FingerDataTransformed.RightHand.Thumb * ThumbAnglesClosed_N.Y, Hierarchy);
			AddRotation("thumb_03_r", FingerDataTransformed.RightHand.Thumb * ThumbAnglesClosed_N.Z, Hierarchy);
		}
		else {
			AddRotation("thumb_01_r", FingerDataTransformed.RightHand.Thumb * Thumb01YAngleClosed, Hierarchy);
			AddRotation("thumb_01_r", FingerDataTransformed.RightHand.Thumb * ThumbAnglesOpen_N.X / ReverseFactor, Hierarchy, "Z");
			AddRotation("thumb_02_r", FingerDataTransformed.RightHand.Thumb * ThumbAnglesOpen_N.Y / ReverseFactor, Hierarchy);
			AddRotation("thumb_03_r", FingerDataTransformed.RightHand.Thumb * ThumbAnglesOpen_N.Z / ReverseFactor, Hierarchy);
		}

		if (FingerDataTransformed.RightHand.Index >= 0.f) {
			AddRotation("index_01_r", FingerDataTransformed.RightHand.Index * IndexAnglesClosed_N.X, Hierarchy);
			AddRotation("index_02_r", FingerDataTransformed.RightHand.Index * IndexAnglesClosed_N.Y, Hierarchy);
			AddRotation("index_03_r", FingerDataTransformed.RightHand.Index * IndexAnglesClosed_N.Z, Hierarchy);
		}
		else {
			AddRotation("index_01_r", FingerDataTransformed.RightHand.Index * IndexAnglesOpen_N.X / ReverseFactor, Hierarchy);
			AddRotation("index_02_r", FingerDataTransformed.RightHand.Index * IndexAnglesOpen_N.Y / ReverseFactor, Hierarchy);
			AddRotation("index_03_r", FingerDataTransformed.RightHand.Index * IndexAnglesOpen_N.Z / ReverseFactor, Hierarchy);
		}

		if (FingerDataTransformed.RightHand.Middle >= 0.f) {
			AddRotation("middle_01_r", FingerDataTransformed.RightHand.Middle * MiddleAnglesClosed_N.X, Hierarchy);
			AddRotation("middle_02_r", FingerDataTransformed.RightHand.Middle * MiddleAnglesClosed_N.Y, Hierarchy);
			AddRotation("middle_03_r", FingerDataTransformed.RightHand.Middle * MiddleAnglesClosed_N.Z, Hierarchy);
		}
		else {
			AddRotation("middle_01_r", FingerDataTransformed.RightHand.Middle * MiddleAnglesOpen_N.X / ReverseFactor, Hierarchy);
			AddRotation("middle_02_r", FingerDataTransformed.RightHand.Middle * MiddleAnglesOpen_N.Y / ReverseFactor, Hierarchy);
			AddRotation("middle_03_r", FingerDataTransformed.RightHand.Middle * MiddleAnglesOpen_N.Z / ReverseFactor, Hierarchy);
		}

		if (FingerDataTransformed.RightHand.Ring >= 0.f) {
			AddRotation("ring_01_r", FingerDataTransformed.RightHand.Ring * RingAnglesClosed_N.X, Hierarchy);
			AddRotation("ring_02_r", FingerDataTransformed.RightHand.Ring * RingAnglesClosed_N.Y, Hierarchy);
			AddRotation("ring_03_r", FingerDataTransformed.RightHand.Ring * RingAnglesClosed_N.Z, Hierarchy);
		}
		else {
			AddRotation("ring_01_r", FingerDataTransformed.RightHand.Ring* RingAnglesOpen_N.X / ReverseFactor, Hierarchy);
			AddRotation("ring_02_r", FingerDataTransformed.RightHand.Ring* RingAnglesOpen_N.Y / ReverseFactor, Hierarchy);
			AddRotation("ring_03_r", FingerDataTransformed.RightHand.Ring* RingAnglesOpen_N.Z / ReverseFactor, Hierarchy);
		}

		if (FingerDataTransformed.RightHand.Pinky >= 0.f) {
			AddRotation("pinky_01_r", FingerDataTransformed.RightHand.Pinky * PinkyAnglesClosed_N.X, Hierarchy);
			AddRotation("pinky_02_r", FingerDataTransformed.RightHand.Pinky * PinkyAnglesClosed_N.Y, Hierarchy);
			AddRotation("pinky_03_r", FingerDataTransformed.RightHand.Pinky * PinkyAnglesClosed_N.Z, Hierarchy);
		}
		else {
			AddRotation("pinky_01_r", FingerDataTransformed.RightHand.Pinky* PinkyAnglesOpen_N.X / ReverseFactor, Hierarchy);
			AddRotation("pinky_02_r", FingerDataTransformed.RightHand.Pinky* PinkyAnglesOpen_N.Y / ReverseFactor, Hierarchy);
			AddRotation("pinky_03_r", FingerDataTransformed.RightHand.Pinky* PinkyAnglesOpen_N.Z / ReverseFactor, Hierarchy);
		}

		//SPLAYS

		float Thumb, Index, Middle, Ring, Pinky;

		Index = 0.7f * -6.0f;
		Middle = 0.3f * 6.0f;
		Thumb = -15.0f;
		Ring = 3.0f;
		Pinky = 7.0f;
		Index = (Index + 0.7f * Index_Middle_Angle_N * FingerData.LeftHand.Index_Middle) * (1.f - FingerData.LeftHand.Index);
		Middle = (Middle + -0.3f * Index_Middle_Angle_N * FingerData.LeftHand.Index_Middle) * (1.f - FingerData.LeftHand.Middle);
		Thumb = (Thumb + Index + Thumb_Index_Angle_N * FingerData.LeftHand.Thumb_Index) * ((1.f - FingerData.LeftHand.Thumb) * (1.f - FingerData.LeftHand.Index));
		Ring = (Ring + Middle - Middle_Ring_Angle_N * FingerData.LeftHand.Middle_Ring) * (1.f - FingerData.LeftHand.Ring);
		Pinky = (Pinky + Ring - Ring_Pinky_Angle_N * FingerData.LeftHand.Ring_Pinky) * (1.f - FingerData.LeftHand.Pinky);

		AddRotation("thumb_01_l", Thumb, Hierarchy, "Z");
		AddRotation("index_01_l", Index, Hierarchy, "Z");
		AddRotation("middle_01_l", Middle, Hierarchy, "Z");
		AddRotation("ring_01_l", Ring, Hierarchy, "Z");
		AddRotation("pinky_01_l", Pinky, Hierarchy, "Z");


		Index = 0.7f * -6.0f;
		Middle = 0.3f * 6.0f;
		Thumb = -15.0f;
		Ring = 3.0f;
		Pinky = 7.0f;
		Index = (Index + 0.7f * Index_Middle_Angle_N * FingerData.RightHand.Index_Middle) * (1.f - FingerData.RightHand.Index);
		Middle = (Middle + -0.3f * Index_Middle_Angle_N * FingerData.RightHand.Index_Middle) * (1.f - FingerData.RightHand.Middle);
		Thumb = (Thumb + Index + Thumb_Index_Angle_N * FingerData.RightHand.Thumb_Index) * ((1.f - FingerData.RightHand.Thumb) * (1.f - FingerData.RightHand.Index));
		Ring = (Ring + Middle - Middle_Ring_Angle_N * FingerData.RightHand.Middle_Ring) * (1.f - FingerData.RightHand.Ring);
		Pinky = (Pinky + Ring - Ring_Pinky_Angle_N * FingerData.RightHand.Ring_Pinky) * (1.f - FingerData.RightHand.Pinky);

		AddRotation("thumb_01_r", Thumb, Hierarchy, "Z");
		AddRotation("index_01_r", Index, Hierarchy, "Z");
		AddRotation("middle_01_r", Middle, Hierarchy, "Z");
		AddRotation("ring_01_r", Ring, Hierarchy, "Z");
		AddRotation("pinky_01_r", Pinky, Hierarchy, "Z");
		

	}
}

FRigUnit_FootLocking::FRigUnit_FootLocking() {

	FloorOffset = 0.f;
	ReleaseHeight = 0.2f;
	ReleaseDistHorizontal = 0.3f;

	LockedLeft = false;
	LockedRight = false;

}

FRigUnit_FootLocking_Execute() {
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_RIGUNIT()

	FRigBoneHierarchy* Bones = ExecuteContext.GetBones();
	FRigControlHierarchy* Controls = ExecuteContext.GetControls();

	if (Bones && Controls) {

		FName FootBoneName;
		bool* Locked = nullptr;
		FTransform* LockedTrans = nullptr;

		for (int i = 0; i < 2; i++) {

			if (i == 0) {
				FootBoneName = "CalfL";
				Locked = &LockedLeft;
				LockedTrans = &LockedTransLeft;
			}
			else {
				FootBoneName = "CalfR";
				Locked = &LockedRight;
				LockedTrans = &LockedTransRight;
			}

			FTransform Root = Bones->GetGlobalTransform("root");
			FTransform Foot = Controls->GetGlobalTransform(FootBoneName);

			FVector RootTrans2D = Root.GetLocation();
			float RootHeight = RootTrans2D.Z;
			RootTrans2D.Z = 0.f;
			FVector FootTrans2D = Root.GetLocation();
			float FootHeight = FootTrans2D.Z;
			FootTrans2D.Z = 0.f;

			if (*Locked) {

				float Dist2D = FVector::Dist(RootTrans2D, FootTrans2D);

				if (FootHeight > RootHeight + FloorOffset + ReleaseHeight || Dist2D > ReleaseDistHorizontal) {
					*Locked = false;
				}
				else {
					Controls->SetGlobalTransform(FootBoneName, *LockedTrans);
				}

			}
			else {

				if (FootHeight < RootHeight + FloorOffset) {
					*Locked = true;
					*LockedTrans = Foot;
				}

			}

		}

	}
}

FRigUnit_FromXYZW::FRigUnit_FromXYZW() {

	X = 0.f;
	Y = 0.f;
	Z = 0.f;
	W = 0.f;
	Quat = FQuat::Identity;

}

FRigUnit_FromXYZW_Execute() {
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_RIGUNIT()

	Quat = FQuat(X, Y, Z, W);
	
}

FRigUnit_AdditionalOffsets::FRigUnit_AdditionalOffsets() {

}

struct OffsetHelperEntry {
	FName name;
	const FRotator* rot;
	const FVector* pos;
	OffsetHelperEntry() { }
	OffsetHelperEntry(const FName& name, const FRotator* rot, const FVector* pos)
		: name(name), rot(rot), pos(pos) {}
};

FRigUnit_AdditionalOffsets_Execute() {
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_RIGUNIT()

	FRigControlHierarchy* Hierarchy = ExecuteContext.GetControls();

	if (Hierarchy) {

		OffsetHelperEntry Offsets[] = {
			OffsetHelperEntry("Head", &AdditionalOffsets.HeadRotOffset, &AdditionalOffsets.HeadPosOffset),
			OffsetHelperEntry("HandL", &AdditionalOffsets.HandLRotOffset, &AdditionalOffsets.HandLPosOffset),
			OffsetHelperEntry("HandR", &AdditionalOffsets.HandRRotOffset, &AdditionalOffsets.HandRPosOffset),
			OffsetHelperEntry("CalfL", &AdditionalOffsets.LowerLegLRotOffset, &AdditionalOffsets.LowerLegLPosOffset),
			OffsetHelperEntry("CalfR", &AdditionalOffsets.LowerLegRRotOffset, &AdditionalOffsets.LowerLegRPosOffset),
			OffsetHelperEntry("LowerArmL", &AdditionalOffsets.LowerArmLRotOffset, &AdditionalOffsets.LowerArmLPosOffset),
			OffsetHelperEntry("LowerArmR", &AdditionalOffsets.LowerArmRRotOffset, &AdditionalOffsets.LowerArmRPosOffset),
			OffsetHelperEntry("Hip", &AdditionalOffsets.LowerBodyRotOffset, &AdditionalOffsets.LowerBodyPosOffset),
			OffsetHelperEntry("Spine03", &AdditionalOffsets.UpperBodyRotOffset, &AdditionalOffsets.UpperBodyPosOffset)
		};

		for (const auto& Offset : Offsets) {
			FTransform Transform = Hierarchy->GetGlobalTransform(Offset.name);

			FQuat NormOffsetRot = (*Offset.rot).Quaternion();
			NormOffsetRot.Normalize();
			FTransform OffsetTransform(NormOffsetRot, *Offset.pos);
			
			Hierarchy->SetGlobalTransform(Offset.name, OffsetTransform * Transform);
		}

	}
}