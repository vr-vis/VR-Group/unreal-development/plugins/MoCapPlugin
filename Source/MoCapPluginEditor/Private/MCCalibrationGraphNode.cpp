// Fill out your copyright notice in the Description page of Project Settings.


#include "MCCalibrationGraphNode.h"

#define LOCTEXT_NAMESPACE "AnimGraphNodes"

UMCCalibrationGraphNode::UMCCalibrationGraphNode(const FObjectInitializer& ObjectInitializer)
    :Super(ObjectInitializer)
{
}

FLinearColor UMCCalibrationGraphNode::GetNodeTitleColor() const
{
    return FLinearColor::Green;
}

FText UMCCalibrationGraphNode::GetTooltipText() const
{
    return LOCTEXT("ApplyCalibrationMoCap", "ApplyCalibrationMoCap");
}

FText UMCCalibrationGraphNode::GetNodeTitle(ENodeTitleType::Type TitleType) const
{
    return LOCTEXT("ApplyCalibrationMoCap", "ApplyCalibrationMoCap");
}

FString UMCCalibrationGraphNode::GetNodeCategory() const
{
    return TEXT("MoCapCalibration");
}

#undef LOCTEXT_NAMESPACE