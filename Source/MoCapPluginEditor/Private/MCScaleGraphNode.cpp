// Fill out your copyright notice in the Description page of Project Settings.


#include "MCScaleGraphNode.h"

#define LOCTEXT_NAMESPACE "AnimGraphNodes"

UMCScaleGraphNode::UMCScaleGraphNode(const FObjectInitializer& ObjectInitializer)
    :Super(ObjectInitializer)
{
}

FLinearColor UMCScaleGraphNode::GetNodeTitleColor() const
{
    return FLinearColor::Green;
}

FText UMCScaleGraphNode::GetTooltipText() const
{
    return LOCTEXT("ApplyScaleMoCap", "ApplyScaleMoCap");
}

FText UMCScaleGraphNode::GetNodeTitle(ENodeTitleType::Type TitleType) const
{
    return LOCTEXT("ApplyScaleMoCap", "ApplyScaleMoCap");
}

FString UMCScaleGraphNode::GetNodeCategory() const
{
    return TEXT("MoCapCalibration");
}

#undef LOCTEXT_NAMESPACE